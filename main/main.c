/* includes ----------------------------------------------------------*/
/* USER CODE BEGIN Includes */
#include "cleaner.h"
#include "global.h"
#include "wifi.h"
#include "pub_sub_client.h"
#include "common_chemins.h"
#include "sensor_rdo206a.h"
#include "sensor_phg206a.h"
#include "sensors_task.h"
#include "servo_clean200.h"
#include "ota.h"
#include "freertos/queue.h"
#include "ring_buffer.h"
#include "servo_clean200.h"
#include "sensors_task.h"
#include "ds1307.h"
#include "esp_task_wdt.h"
/* USER CODE END Includes */

/* Private typedef -----------------------------------------------------------*/
/* USER CODE BEGIN PTD */
typedef enum
{
    eRESET = 0,
    eSET = 1
} BOUY_STATE;
/* USER CODE END PTD */

/* Private define ------------------------------------------------------------*/
/* USER CODE BEGIN PD */
#define MAX_LENGTH 50
#define TWDT_TIMEOUT_MS 20000
#define ESP_INTR_FLAG_DEFAULT 0
/* USER CODE END PD */

/* Private macro -------------------------------------------------------------*/
/* USER CODE BEGIN PM */

/* USER CODE END PM */

/* Private variables ---------------------------------------------------------*/
/* USER CODE BEGIN PV */
const char *MAIN_TAG = "MAIN";

SensorsDef sensors_config;
CheminsDef chemins_config;
uint8_t g_index_queue = 0;
static QueueHandle_t g_publisher_queue, g_subscribe_queue, g_adjust_chemins_queue, g_control_queue;

EventGroupHandle_t g_manager_event;
EventBits_t g_event_manager_bits;
PublishDef mqtt_config;
TaskHandle_t manager_handler, wifi_handler, sensors_handler, cleanup_handler, publish_handler, adjust_chemins_handler;

CleanerDef config =
    {
        .Revolutions = 2,
};
uint8_t g_restart_counter = 0;
char *g_envisor_id;
BOUY_STATE g_bouy_state = eRESET;
bool g_running_system_state = false;
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
/* USER CODE BEGIN PFP */
void System_Init(void)
{
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        ESP_ERROR_CHECK(nvs_flash_erase());
        ret = nvs_flash_init();
    }
    if (ret == ESP_OK) // Tạo phân vùng NVS, lưu trữ cấu hình wifi, broker mqtt,...
    {
        ESP_LOGW(MAIN_TAG, "NVS flash init success");
    }
    else
    {
        ESP_LOGW(MAIN_TAG, "NVS flash init failed");
    }
    ret = nvs_flash_init_partition("nvs_data");
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND)
    {
        ESP_ERROR_CHECK(nvs_flash_erase_partition("nvs_data"));
        ret = nvs_flash_init_partition("nvs_data");
    }
    if (ret == ESP_OK) // Tạo phân vùng NVS, lưu trữ cấu hình wifi, broker mqtt,...
    {
        ESP_LOGW(MAIN_TAG, "NVS data flash init success");
    }
    else
    {
        ESP_LOGW(MAIN_TAG, "NVS data flash init failed");
    }
    nvs_init();
    ESP_ERROR_CHECK(esp_netif_init());                // Thiết lập giao tiếp mạng (Wifi, LAN, PPoPE,...)
    ESP_ERROR_CHECK(esp_event_loop_create_default()); // Thiết lập vòng lặp bắt sự kiện
    esp_netif_create_default_wifi_sta();              // Create the default Wi-Fi station interface
    wifi_stack_init();                                // Khởi tạo stack wifi
    g_publisher_queue = xQueueCreate(100, sizeof(SensorsData));
    g_subscribe_queue = xQueueCreate(2, 200 * sizeof(char));
    g_adjust_chemins_queue = xQueueCreate(2, 200 * sizeof(char));
    g_control_queue = xQueueCreate(2, 200 * sizeof(char));
    g_manager_event = xEventGroupCreate();
    cleanup_init(&config);
    temp_cpu_init(20, 100);
    //     esp_task_wdt_config_t twdt_config = {
    //     .timeout_ms = TWDT_TIMEOUT_MS,
    //     .idle_core_mask = (1 << 2) - 1,    // Bitmask of all cores
    //     .trigger_panic = false,
    // };
    // ESP_ERROR_CHECK(esp_task_wdt_init(&twdt_config));
    // printf("TWDT initialized\n");
    vTaskDelay(1000 / portTICK_PERIOD_MS);
    mqtt_config.EventManager = g_manager_event;
    mqtt_config.PublishQueue = g_publisher_queue;
    sensors_config.SensorsQueue = g_publisher_queue;
    sensors_config.SensorsManager = g_manager_event;
    chemins_config.CheminsManager = g_manager_event;
    chemins_config.CheminsValueQueue = g_adjust_chemins_queue;
    if (g_manager_event == NULL)
    {
        xEventGroupSetBits(g_manager_event, SYSTEM_INIT_FAILED_EVENT);
    }
    else
    {
        xEventGroupSetBits(g_manager_event, SYSTEM_INIT_SUCCESS_EVENT);
    }
}
static void IRAM_ATTR gpio_isr_handler(void *arg)
{
    BaseType_t xHigherPriorityTaskWoken = pdFALSE;
    // ESP_LOGW(MAIN_TAG,"Into interrupt");
    uint32_t gpio_num = (uint32_t)arg;
    xEventGroupSetBitsFromISR(g_manager_event, BOUY_READY_EVENT, &xHigherPriorityTaskWoken);
    portYIELD_FROM_ISR(xHigherPriorityTaskWoken);
}

void gpio_init(void)
{
    gpio_config_t io_conf = {};
    io_conf.intr_type = GPIO_INTR_DISABLE;
    io_conf.mode = GPIO_MODE_OUTPUT;
    io_conf.pin_bit_mask = GPIO_OUTPUT_PIN_SEL;
    io_conf.pull_down_en = 0;
    io_conf.pull_up_en = 0;
    gpio_config(&io_conf);
    // io_conf.intr_type = GPIO_INTR_DISABLE;
    // Set as input
}


void clear_uart_frame(void)
{
    uint8_t clear_frame[20];
    uart_read_bytes(UART_NUM, clear_frame, 20, 100 / portTICK_PERIOD_MS);
}

void shutdown_chemins_system(void)
{
    RDO_OFF();
    RS485_OFF();
}

void turn_on_chemins_system(void)
{
    RDO_ON();
    RS485_ON();
    vTaskDelay(10000 / portTICK_PERIOD_MS); // Wait for the power to be stable
    clear_uart_frame();
}

void bootnum_check()
{
    xEventGroupWaitBits(g_manager_event, SYSTEM_INIT_SUCCESS_EVENT, pdFALSE, pdFALSE, portMAX_DELAY);
    nvs_handle_t my_handle;
    esp_err_t err = nvs_open_from_partition("nvs_data", "storage_data", NVS_READWRITE, &my_handle);
    if (err != ESP_OK)
    {
        ESP_LOGI(MAIN_TAG, "Error (%s) opening NVS handle!\n", esp_err_to_name(err));
    }
    else
    {
        err = nvs_get_u8(my_handle, "restart_counter", &g_restart_counter);
        switch (err)
        {
        case ESP_OK:
            ESP_LOGI(MAIN_TAG, "Restart counter = %d", g_restart_counter);
            break;
        case ESP_ERR_NVS_NOT_FOUND:
            ESP_LOGI(MAIN_TAG, "The value is not initialized yet!\n");
            break;
        default:
            ESP_LOGE(MAIN_TAG, "Error (%s) reading!\n", esp_err_to_name(err));
        }
        // Write
        g_restart_counter++;
        err = nvs_set_u8(my_handle, "restart_counter", g_restart_counter);
        if (err == ESP_OK)
        {
            ESP_LOGI(MAIN_TAG, "Restart counter updated successfully!");
        }
        // Commit written value.
        // After setting any values, nvs_commit() must be called to ensure changes are written
        // to flash storage. Implementations may write to storage at other times,
        // but this is not guaranteed.
        nvs_commit(my_handle);
        nvs_close(my_handle);
    }
}

void manager_task(void *arg)
{
    while (1)
    {

        g_event_manager_bits=xEventGroupWaitBits(g_manager_event,EVENT_WAIT, pdFALSE, pdFALSE, portMAX_DELAY);
        if (g_event_manager_bits & SYSTEM_INIT_SUCCESS_EVENT)
        {
            ESP_LOGI(MAIN_TAG,"System init success");
            xEventGroupClearBits(g_manager_event, SYSTEM_INIT_SUCCESS_EVENT);
            xEventGroupSetBits(g_manager_event,READY_MEASUREMENT_EVENT);
        }
        else if (g_event_manager_bits & SYSTEM_INIT_FAILED_EVENT)
        {
                ESP_LOGI(MAIN_TAG,"System init failed");
                xEventGroupClearBits(g_manager_event, SYSTEM_INIT_FAILED_EVENT);
                esp_restart();
        }
        else if (g_event_manager_bits &FINISH_MEASUREMENT_EVENT)
        {
                xEventGroupClearBits(g_manager_event, FINISH_MEASUREMENT_EVENT);
                xEventGroupSetBits(g_manager_event, READY_ADJUST_EVENT);
        }
        else if (g_event_manager_bits &FINISH_ADJUST_EVENT)
        {
                xEventGroupClearBits(g_manager_event, FINISH_ADJUST_EVENT);
                xEventGroupSetBits(g_manager_event, READY_CLEANER_EVENT);
        }
        else if (g_event_manager_bits & FINISH_CLEANER_EVENT)
        {
            // power_save();
            if (check_newfirmware_exist()==true)
            {
                ESP_LOGW(MAIN_TAG,"Waiting for publishing finish to start OTA...........");
                xEventGroupWaitBits(g_manager_event,FINISH_PUBLISH_EVENT, pdFALSE, pdTRUE, portMAX_DELAY);
                ESP_LOGW(MAIN_TAG,"Aborting now for OTA Starting..............");
                xEventGroupClearBits(g_manager_event, FINISH_CLEANER_EVENT);
                xEventGroupSetBits(g_manager_event, START_OTA_EVENT);
            }
            else
            {
                xEventGroupClearBits(g_manager_event, FINISH_CLEANER_EVENT);
                xEventGroupSetBits(g_manager_event, READY_MEASUREMENT_EVENT);
            }
        }
    }
}


static void handle_water_relay(char *cmd)
{
    uint8_t state = atoi(cmd);
    if (state == 0)
    {
        ESP_LOGI(MAIN_TAG, "Turn water relay off");
        WATER_OFF();
    }
    else
    {
        ESP_LOGI(MAIN_TAG, "Turn water relay on");
        WATER_ON();
    }
}
static void handle_air_relay(char *cmd)
{
    uint8_t state = atoi(cmd);
    if (state == 0)
    {
        ESP_LOGI(MAIN_TAG, "Turn air relay off");
        AIR_OFF();
    }
    else
    {
        ESP_LOGI(MAIN_TAG, "Turn air relay on");
        AIR_ON();
    }
}
static void handle_cleaner_period(char *cmd)
{
    uint8_t period = (uint8_t)atoi(cmd);
    ESP_LOGI(MAIN_TAG, "Set cleaner period: %u", period);
    cleanup_set_period(period);
}

static void handle_wifi(char *cmd)
{
    if (cmd == NULL)
    {
        ESP_LOGW(MAIN_TAG, "Invalid wifi command");
        return;
    }

    char *wifi = strtok(cmd, "/");
    if (wifi == NULL)
    {
        ESP_LOGW(MAIN_TAG, "Invalid SSID format");
        return;
    }

    char ssid_name[50];
    strcpy(ssid_name, wifi);

    char *password = strtok(NULL, "/");
    if (password == NULL)
    {
        ESP_LOGW(MAIN_TAG, "Invalid password format");
        return;
    }

    add_newconfig(ssid_name, password, true);
}
static void handle_reset(char *cmd)
{
    ESP_LOGI(MAIN_TAG, "Resetting the device");
    esp_restart();
}

static void handle_mqtt_id(char *cmd)
{
    if (cmd == NULL)
    {
        ESP_LOGW(MAIN_TAG, "Invalid MQTT ID command");
        return;
    }

    char *broker = strtok(cmd, " ");
    if (broker == NULL)
    {
        ESP_LOGW(MAIN_TAG, "Invalid broker format");
        return;
    }

    char broker_url[50];
    strcpy(broker_url, broker);

    char *id = strtok(NULL, " ");
    if (id == NULL)
    {
        ESP_LOGW(MAIN_TAG, "Invalid ID format");
        return;
    }

    mqtt_set_id(broker_url, id);
}
static void handle_DO(char *cmd)
{
    uint8_t state = atoi(cmd);
    if (state == 0)
    {
        ESP_LOGI(MAIN_TAG, "Turn DO power off");
        RDO_OFF();
    }
    else
    {
        ESP_LOGI(MAIN_TAG, "Turn DO power on");
        RDO_ON();
        vTaskDelay(10000 / portTICK_PERIOD_MS); // Wait for the power to be stable
        clear_uart_frame();
    }
}
static void handle_RS485(char *cmd)
{
    uint8_t state = atoi(cmd);
    if (state == 0)
    {
        ESP_LOGI(MAIN_TAG, "Turn RS485 power off");
        RS485_OFF();
    }
    else
    {
        ESP_LOGI(MAIN_TAG, "Turn RS485 power on");
        RS485_ON();
        vTaskDelay(10000 / portTICK_PERIOD_MS); // Wait for the power to be stable
        clear_uart_frame();
    }
}
static void handle_sync_timestamp(char *cmd)
{
    sync_timestamp();
}
static void handle_set_timestamp(char *cmd)
{
    if (cmd == NULL)
    {
        ESP_LOGW(MAIN_TAG, "Invalid timestamp format");
        return;
    }

    time_t new_timestamp = (time_t)atof(cmd);
    set_timestamp(new_timestamp);
}

typedef void (*command_handler_t)(char *);
typedef struct
{
    const char *command;
    command_handler_t handler;
} command_t;
static const command_t command_table[] = {
    {"water_relay", handle_water_relay},                        // Command to turn the water relay on/off: control/water_relay:1
    {"air_relay", handle_air_relay},                            // Command to turn the air relay on/off: control/air_relay:1
    {"cleaner period", handle_cleaner_period},                  // Command to set the cleaner period: control/cleaner period:5
    {"wifi", handle_wifi},                                      // Command to set the wifi: control/wifi:SSID/password
    {"reset", handle_reset},                                    // Command to reset the device: control/reset
    {"mqtt_id", handle_mqtt_id},                                // Command to set the MQTT ID: control/mqtt_id:broker_url id
    {"DO", handle_DO},                                          // Command to turn the DO power on/off: control/DO:1
    {"RS485", handle_RS485},                                    // Command to turn the RS485 power on/off: control/RS485:1
    {"sync timestamp", handle_sync_timestamp},                  // Command to sync the timestamp: control/sync timestamp
    {"set timestamp", handle_set_timestamp},                    // Command to set the timestamp: control/set timestamp:1620000000
};
void control_task(void *arg)
{
    BaseType_t ret;
    char command[200];
    while (1)
    {
        ret = xQueueReceive(g_control_queue, command, (TickType_t)500);
        if (ret == pdTRUE)
        {
            ESP_LOGI(MAIN_TAG, "Received command data from queue: %s", command);
            char *cmd = strtok(command, ":");
            char *args = strtok(NULL, "");

            // Search the command table for a matching command
            uint8_t command_found_flag = 0;
            for (size_t i = 0; i < sizeof(command_table) / sizeof(command_table[0]); i++)
            {
                if (strcmp(cmd, command_table[i].command) == 0)
                {
                    command_table[i].handler(args);
                    command_found_flag = 1;
                    break;
                }
            }
            if (!command_found_flag)
            {
                ESP_LOGW(MAIN_TAG, "Unknown command: %s !!!", cmd);
            }
        }
    }
}

static void check_bouy_task(void *arg)
{
    uint8_t read_cnt = 0;
    uint32_t logic_bouy = 0;
    uint8_t bouy_set_cnt = 0;
    float bouy_set_percent = 0;
    gpio_config_t io_conf = {};
    io_conf.pin_bit_mask = GPIO_INPUT_PIN_SEL;
    io_conf.mode = GPIO_MODE_INPUT;
    // enable pull-up mode
    io_conf.pull_up_en = 1;
    io_conf.intr_type = GPIO_INTR_ANYEDGE;
    gpio_config(&io_conf);
    gpio_install_isr_service(ESP_INTR_FLAG_DEFAULT);
    // //hook isr handler for specific gpio pin
    gpio_isr_handler_add(GPIO_INPUT_IO_BOUY, gpio_isr_handler, (void *)GPIO_INPUT_IO_BOUY);
    while (1)
    {
        xEventGroupWaitBits(g_manager_event, BOUY_READY_EVENT, pdTRUE, pdTRUE, portMAX_DELAY);
        gpio_intr_disable(GPIO_INPUT_IO_BOUY);
        while (read_cnt < 16)
        {
            uint8_t input_state = (uint8_t)READ_BOUY();
            if (input_state > 0)
            {
                logic_bouy |= 1 << read_cnt;
            }
            else
            {
                logic_bouy &= ~(1 << read_cnt);
            }
            read_cnt++;
            vTaskDelay(10 / portTICK_PERIOD_MS);
        }
        bouy_set_cnt = 0;
        bouy_set_percent = 0;
        read_cnt = 0;
        bouy_set_percent = 0;
        for (int i = 0; i < 16; i++)
        {
            if (logic_bouy & (1 << i))
            {
                bouy_set_cnt++;
            }
        }
        bouy_set_percent = (bouy_set_cnt * 100.0 / 16.0);
        if (bouy_set_percent > 70)
        {
            ESP_LOGW(MAIN_TAG, "Detecting into water");
            g_bouy_state = eSET;
            ESP_LOGW(MAIN_TAG, "Ready to turn on system.....................");
            turn_on_chemins_system();
            g_running_system_state = true;
            xEventGroupSetBits(g_manager_event, SYSTEM_INIT_SUCCESS_EVENT);
            vTaskResume(manager_handler);
            gpio_intr_enable(GPIO_INPUT_IO_BOUY);
        }
        else
        {
            ESP_LOGW(MAIN_TAG, "Detecting out of water");
            g_bouy_state = eRESET;
            ESP_LOGW(MAIN_TAG, "Ready to shut down system because of taking out of water.....................");
            shutdown_chemins_system();
            g_running_system_state = false;
            xEventGroupClearBits(g_manager_event, EVENT_WAIT|READY_MEASUREMENT_EVENT|READY_ADJUST_EVENT|READY_CLEANER_EVENT);
            vTaskSuspend(manager_handler);
            gpio_intr_enable(GPIO_INPUT_IO_BOUY);
        }
    }
}

/* USER CODE END TFH */
void app_main(void)
{
    /* USER CODE BEGIN SysInit */
    System_Init();
    bootnum_check();
    gpio_init();
    vTaskDelay(1000 / portTICK_PERIOD_MS); // Wait for the power to be stable

    //------------client-id----------------
    g_envisor_id = "envisor_testing_07";
    //------------client-id----------------
    // SIM_ON();
    
    ota_init(g_manager_event, g_subscribe_queue);
    /* USER CODE END Init */
    /* USER CODE BEGIN Task */
    add_subscribe_task("ota", g_subscribe_queue);
    add_subscribe_task("chemins", g_adjust_chemins_queue);
    add_subscribe_task("control", g_control_queue);
    subscribe_task(NULL);
    xTaskCreate(wifi_task, "wifi_task", 1024 * 3, g_manager_event, 8, &wifi_handler);
    xTaskCreate(publish_task, "publish_task", 1024 * 4, &mqtt_config, 8, &publish_handler);
    start_ota_task(8);
    xTaskCreate(manager_task, "manager_task", 1024 * 3, NULL, 9, &manager_handler);
    xTaskCreate(sensors_task, "sensors_task", 1024 * 4, &sensors_config, 6, &sensors_handler);
    xTaskCreate(cleanup_task, "clean_task", 1024 * 3, g_manager_event, 6, &cleanup_handler);
    xTaskCreate(adjust_chemins_task, "chemins task", 1024 * 4, &chemins_config, 5, &adjust_chemins_handler);
    // xTaskCreate(clean_mutex, "mutex task", 1024*2, NULL, 5,  NULL);
    xTaskCreate(control_task, "control task", 1024 * 4, NULL, 7, NULL);
    xTaskCreate(publish_frequent_data, "publish frequent data", 1024 * 4, NULL, 5, NULL);
    xTaskCreate(check_bouy_task, "check_bouy task", 1024 * 4, NULL, 10, NULL);
    // xTaskCreate(check_list_tasks, "task_list_task", 1024*3, NULL, 11, NULL);
#if CLEANUP_VERSION > 2
    xTaskCreate(check_bouy_task, "check bouy task", 1024 * 3, NULL, 8, NULL);
#endif
}