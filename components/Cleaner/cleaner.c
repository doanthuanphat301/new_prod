#include "cleaner.h"
#include "internal_cleaner.h"

static CleanerDef *g_cleaner_def;
static EventGroupHandle_t g_cleanup_event;
const char *CLEANER_TAG = "CLEANER";
static uint8_t g_cleaner_counter = 3;
static uint8_t count = 0;
void cleanup_init(CleanerDef *clean_def)
{
    g_cleaner_def = clean_def;
    ESP_LOGI(CLEANER_TAG,"revo0: %d", g_cleaner_def->Revolutions);
#ifdef CLEANUP_VERSION
    g_cleaner_def->Version =CLEANUP_VERSION;
#endif    
#if CLEANUP_VERSION > 1
    gpio_config_t io_conf = {};
    io_conf.intr_type = GPIO_INTR_DISABLE;
    io_conf.mode = GPIO_MODE_OUTPUT;
    io_conf.pin_bit_mask = GPIO_OUTPUT_PIN_SEL;
    io_conf.pull_down_en = 0;
    io_conf.pull_up_en =1; 
    gpio_config(&io_conf);
#ifdef CLEANUP_VERSION > 2
    io_conf.intr_type = GPIO_INTR_DISABLE;
    io_conf.pin_bit_mask = GPIO_INPUT_PIN_SEL;
    //set as input mode
    io_conf.mode = GPIO_MODE_INPUT;
    //enable pull-up mode
    io_conf.pull_up_en = 1;
    gpio_config(&io_conf);
#endif
#endif
    g_cleanup_event=xEventGroupCreate();
}  

void cleanup_set_revolutions(int revolution)
{
    g_cleaner_def->Revolutions = revolution;
    ESP_LOGI(CLEANER_TAG, "set revolutions: %d", g_cleaner_def->Revolutions);
}

void cleanup_discharge_water(bool bouy_check)
{
    if(bouy_check != true)
    {
        cleanup_air_pump(eON);
        vTaskDelay(WAIT_TIME_FOR_FLUSH);
        cleanup_air_pump(eOFF);
    }
    else
    {
        cleanup_air_pump(eON);
        xEventGroupSetBits(g_cleanup_event, READY_CHECK_BOUY_BIT);
        //Wait for signal BOUY_BELOW_SET_BIT
        xEventGroupWaitBits(g_cleanup_event, BOUY_BELOW_SET_BIT, pdTRUE, pdTRUE, portMAX_DELAY);
        vTaskDelay(WAIT_TIME_FOR_FLUSH);
        cleanup_air_pump(eOFF);
    }
}

void cleanup_charge_water(bool bouy_check)
{
    if(bouy_check !=true)
    {
        cleanup_solenoid_valve(eON);
        vTaskDelay(WAIT_TIME_FOR_FILL);
        cleanup_solenoid_valve(eOFF);
    }
    else
    {
        cleanup_solenoid_valve(eON);
        //Wait for signal BOUY_BELOW_SET_BIT
        xEventGroupSetBits(g_cleanup_event, READY_CHECK_BOUY_BIT);
        xEventGroupWaitBits(g_cleanup_event, BOUY_ABOVE_SET_BIT, pdTRUE, pdTRUE, portMAX_DELAY);
        cleanup_air_pump(eOFF);
    }
}

void cleanup_water_pump(CLEANUP_STATE state)
{
    if (state == eON)
    {
        gpio_set_level(g_cleaner_def->WaterPumpPin, 0);
    }
    else
    {
        gpio_set_level(g_cleaner_def->WaterPumpPin, 1);
    }
}

void cleanup_air_pump(CLEANUP_STATE state)
{
    if (state == eON)
    {
        gpio_set_level(g_cleaner_def->AirPumpPin, 0);
    }
    else
    {
        gpio_set_level(g_cleaner_def->AirPumpPin, 1);
    }
}

void cleanup_solenoid_valve(CLEANUP_STATE state)
{
    if (state == eON)
    {
        gpio_set_level(g_cleaner_def->ValvePin, 0);
    }
    else
    {
        gpio_set_level(g_cleaner_def->ValvePin, 1);
    }
}

void cleanup_clean_motor(int revolution)
{
    ESP_LOGI(CLEANER_TAG, "revoooo: %d", revolution);
    clean_set_auto_revolution(UART_NUM, (uint8_t)revolution);
}

#if CLEANUP_VERSION == 1
void cleanupv1_task(void *arg) 
{
    EventGroupHandle_t event_manager = (EventGroupHandle_t) arg;
    while(1)
    {
        //input trigger event
        xEventGroupWaitBits(event_manager, READY_CLEANER_EVENT, pdFALSE, pdTRUE, portMAX_DELAY);
        if (check_need_cleaner() != e_CLEANER_NEED)
        {
            ESP_LOGW(CLEANER_TAG, "Don't need clean");
            xEventGroupClearBits(event_manager, READY_CLEANER_EVENT);
            xEventGroupSetBits(event_manager, FINISH_CLEANER_EVENT); //Send signal to manage_task
            continue;
        }
        else
        {
            ESP_LOGW(CLEANER_TAG,"start clean task");
            cleanup_clean_motor((int)g_cleaner_def->Revolutions);
            ESP_LOGI(CLEANER_TAG,"clean 200 is running");
            vTaskDelay(20000/ portTICK_PERIOD_MS);
            ESP_LOGW(CLEANER_TAG,"finished clean task");
            //trigger event output
            xEventGroupClearBits(event_manager, READY_CLEANER_EVENT);
            // if(manager_bit &READY_OTA_EVENT)
            // {
            //     xEventGroupClearBits(event_manager,READY_OTA_EVENT);
            //     ESP_LOGI(CLEANER_TAG,"Aborting task for OTA MODE");
            //     xEventGroupSetBits(event_manager, START_OTA_EVENT);
            // }
            // else
            // {
                xEventGroupSetBits(event_manager, FINISH_CLEANER_EVENT); //Send signal to manage_task
            // }

        }
    }
}
#elif CLEANUP_VERSION == 2
void cleanupv2_task(void *arg)
{
     EventGroupHandle_t event_manager = (EventGroupHandle_t) arg;
    while(1)
    {
        xEventGroupWaitBits(event_manager, READY_CLEANER_EVENT, pdFALSE, pdTRUE, portMAX_DELAY);
        if(check_need_cleaner()!=e_CLEANER_NEED)
        {
            ESP_LOGI(CLEANER_TAG,"finished clean task");
            xEventGroupClearBits(event_manager, READY_CLEANER_EVENT);
            xEventGroupSetBits(event_manager, FINISH_CLEANER_EVENT); //Send signal to manage_task
            continue;
        }
        else
        {
            cleanup_discharge_water(false);
            cleanup_clean_motor(g_cleaner_def->Revolutions);
            cleanup_charge_water(false);
             xEventGroupClearBits(event_manager, READY_CLEANER_EVENT);
             xEventGroupSetBits(event_manager, FINISH_CLEANER_EVENT);
        }
    }

}
#elif CLEANUP_VERSION == 3 
void cleanupv3_task(void *arg)
{
    EventGroupHandle_t event_manager = (EventGroupHandle_t) arg;
    while(1)
    {
        xEventGroupWaitBits(g_cleanup_event, READY_CLEANER_EVENT, pdTRUE, pdTRUE, portMAX_DELAY);
        if(check_need_cleaner()!=e_CLEANER_NEED)
        {
            ESP_LOGI(CLEANER_TAG,"finished clean task");
             xEventGroupClearBits(g_manager_event, READY_CLEANER_EVENT);
            xEventGroupSetBits(g_cleanup_event, FINISH_CLEANER_EVENT); //Send signal to manage_task
            continue;
        }
        else
        {
            cleanup_discharge_water(true);
            cleanup_clean_motor(g_cleaner_def->Revolutions);
            cleanup_charge_water(true);
            xEventGroupClearBits(g_manager_event, READY_CLEANER_EVENT);
            xEventGroupSetBits(event_manager, FINISH_CLEANER_EVENT);
        }
    }
}
#elif CLEANUP_VERSION == 4
void cleanupv4_task(void *arg)
{
     EventGroupHandle_t event_manager = (EventGroupHandle_t) arg;
    while(1)
    {
        xEventGroupWaitBits(event_manage, READY_CLEANER_EVENT, pdTRUE, pdTRUE, portMAX_DELAY);
        if(check_need_cleaner()!=e_CLEANER_NEED)
        {
            ESP_LOGI(CLEANER_TAG,"finished clean task");
             xEventGroupClearBits(g_manager_event, READY_CLEANER_EVENT);
            xEventGroupSetBits(event_manage, FINISH_CLEANER_EVENT); //Send signal to manage_task
            continue;
        }
        else
        {
            cleanup_discharge_water(true);
            cleanup_water_pump(eON);
            cleanup_clean_motor(g_cleaner_def->Revolutions);
            vTaskDelay(WAIT_TIME_FOR_CLEANUP);
            cleanup_water_pump(eOff);
            cleanup_charge_water(true);
             xEventGroupClearBits(g_manager_event, READY_CLEANER_EVENT);
            xEventGroupSetBits(event_manage, FINISH_CLEANER_EVENT); //Send signal to manage_task
        }
    }    
}
#elif CLEANUP_VERSION == 5 
void cleanupv5_task(void *arg)
{
     EventGroupHandle_t event_manager = (EventGroupHandle_t) arg;
    while(1)
    {
        xEventGroupWaitBits(event_manage, READY_CLEANER_EVENT, pdTRUE, pdTRUE, portMAX_DELAY);
        if(check_need_cleaner()!=e_CLEANER_NEED)
        {
            ESP_LOGI(CLEANER_TAG,"finished clean task");
             xEventGroupClearBits(g_manager_event, READY_CLEANER_EVENT);
            xEventGroupSetBits(event_manage, FINISH_CLEANER_EVENT); //Send signal to manage_task
            continue;
        }
        else
        {
            cleanup_discharge_water(true);
            cleanup_water_pump(eON);
            vTaskDelay(WAIT_TIME_FOR_CLEANUP);
            cleanup_water_pump(eOff);
            cleanup_charge_water(true);
             xEventGroupClearBits(g_manager_event, READY_CLEANER_EVENT);
            xEventGroupSetBits(event_manage, FINISH_CLEANER_EVENT); //Send signal to manage_task
        }
    }    
}
#endif
void cleanup_set_period(uint8_t numbers_of_measurements)
{
    g_cleaner_counter = numbers_of_measurements;
    count = 0;
    printf("period: %u", g_cleaner_counter);
}
CLEANER_STATE check_need_cleaner(void)
{
    //If you want to clean
    count++;
    if(count ==g_cleaner_counter)
    {
        count=0;
        return e_CLEANER_NEED;
    }
    return e_CLEANER_NOT_NEED;
}

#if CLEANUP_VERSION > 2
void check_bouy_task(void *arg)
{
    while(1)
    {
        xEventGroupWaitBits(g_cleanup_event,READY_CHECK_BOUY_BIT, pdFALSE, pdTRUE, portMAX_DELAY);
        if(READ_STATE_BOUY_BLOW()==BOUY_BELOW_SET)
        {
            xEventGroupSetBits(g_cleanup_event, BOUY_BELOW_SET_BIT);
            ESP_LOGI(CLEANER_TAG,"Boy below is set");
            xEventGroupClearBits(g_cleanup_event,READY_CHECK_BOUY_BIT);
            xEventGroupClearBits(g_cleanup_event,NO_BOUY_SET_BIT);
            continue;
        }
        if (READ_STATE_BOUY_ABOVE()==BOUY_ABOVE_SET)
        {
            xEventGroupSetBits(g_cleanup_event, BOUY_ABOVE_SET_BIT);
            ESP_LOGI(CLEANER_TAG,"Boy above is set");
            xEventGroupClearBits(g_cleanup_event,READY_CHECK_BOUY_BIT);
            xEventGroupClearBits(g_cleanup_event,NO_BOUY_SET_BIT);
            continue;

        }
        else
        {
            vTaskDelay(10/portTICK_PERIOD_MS);
            xEventGroupClearBits(g_cleanup_event,BOUY_ABOVE_SET_BIT);
            xEventGroupClearBits(g_cleanup_event,BOUY_BELOW_SET_BIT);
            xEventGroupSetBits(g_cleanup_event, NO_BOUY_SET_BIT);
            continue;

        }
    }
}
#endif

void cleanup_task(void *arg)
{
#if CLEANUP_VERSION ==   1
cleanupv1_task(arg);
#elif CLEANUP_VERSION == 2
cleanupv2_task(arg);
#elif CLEANUP_VERSION == 3
cleanupv3_task(arg);
#elif CLEANUP_VERSION == 4
cleanupv4_task(arg);
#elif CLEANUP_VERSION == 5
cleanupv5_task(arg);
#endif
}