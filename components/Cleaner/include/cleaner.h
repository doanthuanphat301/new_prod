#ifndef CLEANER_H
#define CLEANER_H

#include "global.h"
#include <stdint.h>
#include "servo_clean200.h"

#define READ_STATE_BOUY_ABOVE() gpio_get_level(g_cleaner_def->BuoyAbovePin)
#define READ_STATE_BOUY_BLOW()  gpio_get_level(g_cleaner_def->BuoyBelowPin)

#define BOUY_ABOVE_SET          0
#define BOUY_BELOW_SET          1

#define WAIT_TIME_FOR_FLUSH     5000/ portTICK_PERIOD_MS
#define WAIT_TIME_FOR_FILL      5000/ portTICK_PERIOD_MS
#define WAIT_TIME_FOR_CLEANUP   30000/ portTICK_PERIOD_MS
#define DEBOUND_TIME            10/portTICK_PERIOD_MS

#if CLEANUP_VERSION == 2
#define GPIO_OUTPUT_PIN_SEL     ((1ULL<<g_cleaner_def->AirPumpPin) | (1ULL<<g_cleaner_def->ValvePin) 

#elif CLEANUP_VERSION == 3
#define GPIO_OUTPUT_PIN_SEL     ((1ULL<<g_cleaner_def->AirPumpPin) | (1ULL<<g_cleaner_def->ValvePin) 

#define GPIO_INPUT_PIN_SEL       ((1ULL<<g_cleaner_def->BuoyAbovePin) | (1ULL<<g_cleaner_def->BuoyBelowPin))


#elif CLEANUP_VERSION == 4 || CLEANUP_VERSION == 5
#define GPIO_OUTPUT_PIN_SEL     ((1ULL<<g_cleaner_def->AirPumpPin) | (1ULL<<g_cleaner_def->ValvePin) \
                                |(1ULL<<g_cleaner_def->WaterPumpPin)

#define GPIO_INPUT_PIN_SEL       ((1ULL<<g_cleaner_def->BuoyAbovePin) | (1ULL<<g_cleaner_def->BuoyBelowPin))
#endif


typedef enum
{
    eON =  1,
    eOFF = 0,
}CLEANUP_STATE;

typedef struct 
{
    uint8_t Version;
    int    Revolutions;
    uint8_t AirPumpPin;
    uint8_t ValvePin;
    uint8_t WaterPumpPin;
    uint8_t BuoyAbovePin;
    uint8_t BuoyBelowPin;
}CleanerDef;

typedef enum
{ 
    e_CLEANER_OK        = 0,
    e_CLEANER_FAIL      =1,
    e_CLEANER_NEED      =2,
    e_CLEANER_NOT_NEED  =3
}CLEANER_STATE;

void cleanup_init(CleanerDef *clean_def);
void cleanup_discharge_water(bool bouy_check);
void cleanup_charge_water(bool bouy_check);
void cleanup_water_pump(CLEANUP_STATE state);
void cleanup_air_pump(CLEANUP_STATE state);
void cleanup_solenoid_valve(CLEANUP_STATE state);
void cleanup_clean_motor(int revolution);
CLEANER_STATE check_need_cleaner(void);
void cleanup_set_revolutions(int revolution);
void cleanup_set_period(uint8_t numbers_of_measurements);
#if CLEANUP_VERSION >2
void check_bouy_task(void *arg);
#endif

#if CLEANUP_VERSION == 1
void cleanupv1_task(void *arg); //This version only use clean motor
#elif CLEANUP_VERSION == 2
void cleanupv2_task(void *arg); //This version additionally use discharge and charge water process without checking bouy
#elif CLEANUP_VERSION == 3 
void cleanupv3_task(void *arg); //This version like v2 but having checking bouy process
#elif CLEANUP_VERSION == 4
void cleanupv4_task(void *arg); //This version additionally use water pump
#elif CLEANUP_VERSION == 5 
void cleanupv5_task(void *arg); //This version like v4 but not using clean motor
#endif

void cleanup_task(void *arg);

#endif // CLEANER_H