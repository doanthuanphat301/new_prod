#ifndef INTERNAL_CLEANER_H
#define INTERNAL_CLEANER_H

#include "global.h"

#define READY_CHECK_BOUY_BIT    BIT0
#define NO_BOUY_SET_BIT         BIT1
#define BOUY_ABOVE_SET_BIT      BIT2
#define BOUY_BELOW_SET_BIT      BIT3

#endif // INTERNAL_CLEANER_H