#ifndef DEVICEPROVISIONING_H
#define DEVICEPROVISIONING_H

#include "global.h"
#include "wifi.h"

#define MAX_HTTP_RECV_BUFFER 512
#define MAX_HTTP_OUTPUT_BUFFER 1028

char* device_register(void);

#endif // DEVICEPROVISIONING_H