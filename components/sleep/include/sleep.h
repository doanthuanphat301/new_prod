#ifndef SLEEP_H
#define SLEEP_H

#include <time.h>
#include "esp_check.h"
#include "esp_sleep.h"
#include "esp_log.h"
#include "esp_timer.h"

esp_err_t register_timer_wakeup(int timer_sleep_in_s);
void sleep_init(int sleep_time);
void go_to_sleep(void);
void calculateRunningTime(void (*func)(void), char *func_name);

#endif // SLEEP_H