#include <stdio.h>
#include "sleep.h"
#include "driver/uart.h"

static const char *TAG = "timer_wakeup";

/**
 * @brief Registers the timer as a wakeup source.
 *
 * This function configures the timer to wake up the system after a specified sleep time.
 *
 * @param[in] timer_sleep_in_s Sleep time in seconds.
 * @return ESP_OK if successful, or an error code if the configuration fails.
 */
esp_err_t register_timer_wakeup(int timer_sleep_in_s)
{
    int sleep_time = timer_sleep_in_s * 1000 * 1000;
    ESP_RETURN_ON_ERROR(esp_sleep_enable_timer_wakeup(sleep_time), TAG, "Configure timer as wakeup source failed");
    ESP_LOGI(TAG, "timer wakeup source is ready");
    return ESP_OK;
}
/**
 * @brief Initializes sleep mode with a timer wakeup.
 *
 * This function sets up the system to enter sleep mode, waking up after the specified sleep time.
 *
 * @param[in] sleep_time Sleep time in seconds.
 */
void sleep_init(int sleep_time)
{
    ESP_ERROR_CHECK(register_timer_wakeup(sleep_time));
}
/**
 * @brief Puts the system into light sleep mode.
 *
 * This function performs the following steps:
 * 1. Prints "Sleep!!" to the console.
 * 2. Waits for the UART transmission to complete (if applicable).
 * 3. Records the current time.
 * 4. Initiates light sleep mode.
 * 5. Calculates and prints the sleep time in milliseconds.
 */
void go_to_sleep(void)
{
    printf("Sleep!!\n");  // Print a message indicating sleep mode
    uart_wait_tx_idle_polling(CONFIG_ESP_CONSOLE_UART_NUM);  // Wait for UART transmission to complete
    int time_before_sleep = esp_timer_get_time();  // Record current time
    esp_light_sleep_start();  // Enter light sleep mode
    int time_after_sleep = esp_timer_get_time();  // Record time after waking up
    printf("Sleep time: %d ms\n", (time_after_sleep - time_before_sleep) / 1000);  // Calculate and print sleep time
}
/**
 * @brief Measures the running time of a given function.
 *
 * This function records the start time, executes the specified function, records the end time,
 * and calculates the time difference in milliseconds. It then prints the running time along with
 * the provided function name.
 *
 * @param[in] func Pointer to the function to be measured.
 * @param[in] func_name Name of the function (for display purposes).
 */
void calculateRunningTime(void (*func)(void), char *func_name) 
{
    // Record the start time
    //int time_before_running = esp_timer_get_time();
    
    // Execute the function
    func();
    
    // Record the end time
    //int time_after_running = esp_timer_get_time();
    
    // Calculate the time difference in milliseconds
    //printf("%s running time: %d ms\n", func_name, (time_after_running - time_before_running) / 1000);
}