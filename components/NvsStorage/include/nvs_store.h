#ifndef NVS_STORE_H
#define NVS_STORE_H

#include "global.h"
#include "common_chemins.h"

#define STORAGE_NAMESPACE   "storage_data"
#define MAX_STORAGE_SIZE       6000
#define MAX_KEY_LENGTH          16


typedef union
{
    uint64_t Frame;
    uint8_t Data[8];
}DataStore;

typedef enum
{
    eNVS_EMPTY = 0,
    eNVS_READY = 1,
    eNVS_FULL  = 2,
    eNVS_ERROR = 3,
}NVS_STATE;

typedef enum
{
    eNVS_WRITE = 0,
    eNVS_READ  = 1,
}NVS_ACTION;
// static uint64_t g_number_of_key =0;
// static uint64_t g_number_of_read_keys =0;

// NVS_STATE nvs_check_state(void);
esp_err_t nvs_store_commit(nvs_handle_t handle,NVS_ACTION action);
esp_err_t nvs_init(void);
DataStore nvs_transfer_data_to_storage(SensorsData data);
SensorsData nvs_transfer_storage_to_data(DataStore storage);
esp_err_t nvs_store(DataStore store_data);
esp_err_t nvs_store_again(DataStore store_data);
NVS_STATE nvs_load(DataStore *data);
uint16_t nvs_get_tail(void);
#endif
