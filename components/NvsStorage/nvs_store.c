#include "nvs_store.h"
#include <string.h>

static uint16_t head_iterator = 0;
static uint16_t tail_iterator = 0;
static const char * NVS_TAG ="NVS";
esp_err_t nvs_init(void)
{
    nvs_handle my_handle;
    esp_err_t err;
    printf("Opening Non-Volatile Storage...... ");
    err = nvs_open_from_partition("nvs_data",STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK)
    {
        ESP_LOGE(NVS_TAG,"Failed to open NVS handle");
        return err;
    }
    ESP_LOGI(NVS_TAG,"Reading head and tail pointers....");
    head_iterator = 0;
    err = nvs_get_u16(my_handle, "head_iterator", &head_iterator);
        switch (err)
        {
            case ESP_OK:
                printf("Done\n");
                printf("Head_iterator = %" PRIu16 "\n", head_iterator);
                break;
            case ESP_ERR_NVS_NOT_FOUND:
                printf("The value head iterator is not initialized yet!\n");
                break;
            default :
                printf("Error (%s) reading!\n", esp_err_to_name(err));
                return err;
        }
    tail_iterator =0;
    err = nvs_get_u16(my_handle, "tail_iterator",&tail_iterator);
        switch (err)
        {
            case ESP_OK:
                printf("Done\n");
                printf("Tail_iterator = %" PRIu16 "", tail_iterator);
                break;
            case ESP_ERR_NVS_NOT_FOUND:
                printf("The value tail iterator is not initialized yet!\n");
                break;
            default :
                printf("Error (%s) reading!\n", esp_err_to_name(err));
                return err;
        }
    return ESP_OK;
}

esp_err_t nvs_store_commit(nvs_handle_t handle,NVS_ACTION action)
{
    esp_err_t err;
    if(action == eNVS_WRITE)
    {
        err = nvs_set_u16(handle,"head_iterator",head_iterator);
        if (err != ESP_OK)
        {
            ESP_LOGE(NVS_TAG,"Failed to store data");
            if(err ==ESP_ERR_NVS_NOT_ENOUGH_SPACE )
            {
                ESP_LOGE(NVS_TAG,"Not enough space in NVS!");
            }
            return err;
        }
        else
        {
            uint16_t size  =(head_iterator - tail_iterator + MAX_STORAGE_SIZE)%MAX_STORAGE_SIZE;
            ESP_LOGI(NVS_TAG,"Data in NVS :%" PRIu16 " ",size);
            ESP_LOGI(NVS_TAG,"Head iterator[%" PRIu16 "]",head_iterator);
            ESP_LOGI(NVS_TAG,"tail iterator[%" PRIu16 "]",tail_iterator);
        }
    }
    else
    {
        err = nvs_set_u16(handle,"tail_iterator",tail_iterator);
        if (err != ESP_OK)
        {
            ESP_LOGE(NVS_TAG,"Failed to store data");
            if(err ==ESP_ERR_NVS_NOT_ENOUGH_SPACE )
            {
                ESP_LOGE(NVS_TAG,"Not enough space in NVS!");
            }
            return err;
        }
        else
        {
            uint16_t size  =(head_iterator - tail_iterator + MAX_STORAGE_SIZE)%MAX_STORAGE_SIZE;
            ESP_LOGI(NVS_TAG,"Data in NVS :%" PRIu16 " ",size);
            ESP_LOGI(NVS_TAG,"Head iterator[%" PRIu16 "]",head_iterator);
            ESP_LOGI(NVS_TAG,"tail iterator[%" PRIu16 "]",tail_iterator);
        }
    }

    err = nvs_commit(handle);
    if (err != ESP_OK)
    {
        ESP_LOGE(NVS_TAG,"Failed to commit");
        return err;
    }
    return ESP_OK;
}
esp_err_t nvs_store(DataStore store_data)
{
     nvs_handle_t my_handle;
    esp_err_t err;

    err = nvs_open_from_partition("nvs_data",STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK)
    {
        ESP_LOGE(NVS_TAG,"Failed to open NVS handle");
        return err;
    }
    uint64_t data = store_data.Frame;
    int next;
    next = head_iterator +1;
    if(next >= MAX_STORAGE_SIZE)
    {
        next =0;
    }
    if (next  == tail_iterator)
    {
        ESP_LOGE(NVS_TAG,"Storage full, ready to overwrite........");
        DataStore erase_data;
        nvs_load(&erase_data);
    }
    char key[16];
    snprintf(key,sizeof(key),"key:%" PRIu16 "", head_iterator);
    err = nvs_set_u64(my_handle,key,data);
    if (err != ESP_OK)
    {
        ESP_LOGE(NVS_TAG,"Failed to store data");
        if(err ==ESP_ERR_NVS_NOT_ENOUGH_SPACE )
        {
            ESP_LOGE(NVS_TAG,"Not enough space in NVS!");
        }
        return err;
    }
    else
    {
        ESP_LOGI(NVS_TAG,"Store data successfully index[%" PRIu16 "]: %" PRIu64 "",head_iterator,store_data.Frame);
    }
    head_iterator = next;
    nvs_store_commit(my_handle,eNVS_WRITE);
    if (err != ESP_OK)
    {
        ESP_LOGE(NVS_TAG,"Failed to commit");
        return err;
    }
    // Close
    nvs_close(my_handle);
    return ESP_OK;

}

NVS_STATE nvs_load(DataStore *data)
{
    nvs_handle_t my_handle;
    esp_err_t err;
    char key[MAX_KEY_LENGTH];
    err = nvs_open_from_partition("nvs_data",STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK)
    {
        ESP_LOGE(NVS_TAG,"Failed to open NVS handle");
        return eNVS_ERROR;
    }

    uint64_t next;
    if(head_iterator == tail_iterator) // if head =tail, we don't have any data
    {
        // for (uint16_t i = 0; i < MAX_STORAGE_SIZE; i++)
        // {
        //     snprintf(key, sizeof(key), "key:%"PRIu16"\n", i);

        //     err = nvs_get_u64(my_handle, key, &(data->Frame));
        //     if (err == ESP_OK)
        //     {
        //         ESP_LOGI(NVS_TAG,"Data %d: %" PRIu64 "\n ",i,data->Frame);
        //         nvs_erase_key(my_handle, key);
        //         tail_iterator = i ;
        //         err=nvs_store_commit(my_handle,eNVS_READ);
        //         if (err!= ESP_OK)
        //         {
        //             ESP_LOGE(NVS_TAG,"Failed to commit");
        //             return eNVS_ERROR;
        //         }
        //         nvs_close(my_handle);
        //         return eNVS_READY;
        //     }
        //     }
            nvs_close(my_handle);
            ESP_LOGW(NVS_TAG,"Don't have any storage data");
            return eNVS_EMPTY;
    }
    next = tail_iterator +1;
    if(next >=MAX_STORAGE_SIZE)
    {
        next =0;
    }
    sprintf(key, "key:%" PRIu16 "", tail_iterator);
    err = nvs_get_u64(my_handle, key, &(data->Frame));
    if (err!= ESP_OK && err!= ESP_ERR_NVS_NOT_FOUND)
    {
        printf("failed to get data value\n");
        return eNVS_ERROR;
    }
    else
    {
        printf("data value %" PRIu16 " = %" PRIu64 "\n",tail_iterator, data->Frame);
        nvs_erase_key(my_handle, key);
    }
    tail_iterator = next;
    nvs_store_commit(my_handle,eNVS_READ);
    // Close
    nvs_close(my_handle);
    return eNVS_READY;
}



DataStore nvs_transfer_data_to_storage(SensorsData data)
{
    DataStore Storage;
    data.Rdo206AValue.Temp = (uint8_t)(data.Rdo206AValue.Temp * 5);
    data.Rdo206AValue.DO   = (uint8_t)(data.Rdo206AValue.DO * 10);
    data.Phg206AValue.Temp = (uint8_t)(data.Phg206AValue.Temp * 5);
    data.Phg206AValue.PH  = (uint8_t)(data.Phg206AValue.PH * 10);

    Storage.Data[4] = data.TimeStamp;
    Storage.Data[5] = (data.TimeStamp >> 8);
    Storage.Data[6] = (data.TimeStamp >> 16);
    Storage.Data[7] = (data.TimeStamp >> 24);
    Storage.Data[3] = data.Rdo206AValue.Temp;
    Storage.Data[2] = data.Rdo206AValue.DO;
    Storage.Data[1] = data.Phg206AValue.Temp;
    Storage.Data[0] = data.Phg206AValue.PH;
    return Storage;
}

SensorsData nvs_transfer_storage_to_data(DataStore storage)
{
    SensorsData Data;
    Data.TimeStamp = ((uint32_t)storage.Data[4] | ((uint32_t)storage.Data[5] << 8) | ((uint32_t)storage.Data[6] << 16) | ((uint32_t)storage.Data[7] << 24));
    Data.Rdo206AValue.Temp = storage.Data[3] / 5.0;
    Data.Rdo206AValue.DO = storage.Data[2] / 10.0;
    Data.Phg206AValue.Temp = storage.Data[1] / 5.0;
    Data.Phg206AValue.PH = storage.Data[0] / 10.0;
    printf("Data: %" PRId32 " %0.1f %0.2f %0.1f %0.2f\n", Data.TimeStamp, Data.Rdo206AValue.Temp, Data.Rdo206AValue.DO, Data.Phg206AValue.Temp, Data.Phg206AValue.PH);
    return Data;
}


esp_err_t nvs_store_again(DataStore store_data)
{
    nvs_handle_t my_handle;
    esp_err_t err;

    err = nvs_open_from_partition("nvs_data",STORAGE_NAMESPACE, NVS_READWRITE, &my_handle);
    if (err != ESP_OK)
    {
        ESP_LOGE(NVS_TAG,"Failed to open NVS handle");
        return err;
    }
    uint64_t data = store_data.Frame;
    char key[16];
    tail_iterator--;
    snprintf(key,sizeof(key),"key:%" PRIu16 "", tail_iterator);
    err = nvs_set_u64(my_handle,key,data);
    if (err != ESP_OK)
    {
        ESP_LOGE(NVS_TAG,"Failed to store data");
        if(err ==ESP_ERR_NVS_NOT_ENOUGH_SPACE )
        {
            ESP_LOGE(NVS_TAG,"Not enough space in NVS!");
        }
        return err;
    }
    else
    {
        ESP_LOGI(NVS_TAG,"Store data successfully index[%" PRIu16 "]: %" PRIu64 "",tail_iterator,store_data.Frame);
    }
    nvs_store_commit(my_handle,eNVS_WRITE);
    if (err != ESP_OK)
    {
        ESP_LOGE(NVS_TAG,"Failed to commit");
        return err;
    }
    // Close
    nvs_close(my_handle);
    return ESP_OK;
}

uint16_t nvs_get_tail(void)
{
    return tail_iterator;
}