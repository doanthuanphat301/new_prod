#include "global.h"
#include "internal_sensors.h"
#include "sensor_phg206a.h"
#include "sensor_rdo206a.h"
#include "common_chemins.h"
#include "sensors_task.h"
#include "cleaner.h"
#include "wifi.h"

const char *SENSORS_TASK = "SENSORS_TASK";
const char *ADJUST_TASK = "ADJUST_TASK";

SensorsDef *g_sensors_def;
SensorsData g_sensor={0};
Phg206a g_phg206a_data={0,0,0,0,0};
Rdo206a g_rdo206a_data={0,0,0,0,0,0};
EventGroupHandle_t g_sensors_event, g_adjust_chemins_event;
TaskHandle_t g_sensor_task_handle;

static uint64_t g_measurement_time = 60000;         // Default measurement time is 60 seconds
static esp_timer_handle_t periodic_timer;
static uint32_t g_unix_get_timestamp = 0;

static void periodic_timer_callback(void *arg)
{
    // Notify the sensor task
    xTaskNotifyGive(g_sensor_task_handle);
}

void start_esp_timer()
{
    esp_timer_create_args_t periodic_timer_args = {
        .callback = &periodic_timer_callback,
        .name = "periodic_sensors_task_timer"
    };

    esp_err_t err = esp_timer_create(&periodic_timer_args, &periodic_timer);
    
    if (err != ESP_OK)
    {
        ESP_LOGE(SENSORS_TASK, "Failed to create periodic timer!!!");
        return;
    }
    
    err = esp_timer_start_periodic(periodic_timer, g_measurement_time * 1000); // Convert to microseconds
    if (err != ESP_OK)
    {
        ESP_LOGE(SENSORS_TASK, "Failed to start periodic timer!!!");
        esp_timer_delete(periodic_timer);
        return;
    }

    ESP_LOGI(SENSORS_TASK, "Start Periodic Timer successfully: %lld seconds!!!", g_measurement_time);
}

void stop_esp_timer()
{
    esp_timer_stop(periodic_timer);
    esp_timer_delete(periodic_timer);
    periodic_timer = NULL;
    ESP_LOGI(SENSORS_TASK, "Stop and delete periodic timer!!!");
}

void sensors_init()
{
    RDO_ON();
    RS485_ON();
    vTaskDelay(10000 / portTICK_PERIOD_MS);     // Wait for the power to be stable
    ESP_LOGI(SENSORS_TASK, "Wait 10 seconds for the power to be stable!\n");
    chemins_uart_init(UART_NUM, TXD_PIN, RXD_PIN, BUF_SIZE * 2, BUF_SIZE * 2);

    g_sensors_event=xEventGroupCreate();
    if (g_sensors_event==NULL)
    {
        ESP_LOGE(SENSORS_TASK, "Failed to create sensors event group!");
        return;
    }
    start_esp_timer();
}

void sensors_task(void *arg)
{
    SensorsDef *def = (SensorsDef *)arg;
    g_sensor_task_handle = xTaskGetCurrentTaskHandle();
    sensors_init();
    while (1)
    {
        xEventGroupWaitBits(def->SensorsManager, READY_MEASUREMENT_EVENT, pdFALSE, pdTRUE, portMAX_DELAY);
        ulTaskNotifyTake(pdTRUE, (TickType_t)portMAX_DELAY);        // Wait for the notification from the timer callback

        ESP_LOGW(SENSORS_TASK, "Into sensors task!\n");
        if (rdo206a_turn_on(UART_NUM) == eCHEMINS_OK)               // Power on RDO206A
        {
            vTaskDelay(0.5 * g_measurement_time / portTICK_PERIOD_MS);
            if(rdo206a_read_measurement(UART_NUM) ==  eCHEMINS_OK )
            {
                rdo206a_get_data(RDO206A_DO_e, &g_rdo206a_data);
                rdo206a_get_data(RDO206A_TEMP_e, &g_rdo206a_data);
                g_sensor.Rdo206AValue.Temp = g_rdo206a_data.Temp;
                g_sensor.Rdo206AValue.DO = g_rdo206a_data.DO;
                rdo206a_turn_off(UART_NUM);                         // Power off RDO206A

                xEventGroupClearBits(g_sensors_event, DATA_RDO206A_FAILED_BIT);
                xEventGroupSetBits(g_sensors_event, DATA_RDO206A_SUCCESS_BIT);
            }
        } 
        else
        {
            g_sensor.Rdo206AValue.Temp = DATA_ERROR;
            g_sensor.Rdo206AValue.DO = DATA_ERROR;
            xEventGroupClearBits(g_sensors_event, DATA_RDO206A_SUCCESS_BIT);
            xEventGroupSetBits(g_sensors_event, DATA_RDO206A_FAILED_BIT);
        }

        if (phg206a_read_measurement(UART_NUM) == eCHEMINS_OK)
        {
            phg206a_get_data(PHG206A_PH_e, &g_phg206a_data);
            phg206a_get_data(PHG206A_TEMP_e, &g_phg206a_data);
            g_sensor.Phg206AValue.PH = g_phg206a_data.PH;
            get_cpu_temp(&g_sensor.Phg206AValue.Temp);
            xEventGroupClearBits(g_sensors_event, DATA_PHG206A_FAILED_BIT);
            xEventGroupSetBits(g_sensors_event, DATA_PHG206A_SUCCESS_BIT);
        }
        else
        {
            g_sensor.Phg206AValue.PH = DATA_ERROR;
            get_cpu_temp(&g_sensor.Phg206AValue.Temp);
            xEventGroupClearBits(g_sensors_event, DATA_PHG206A_SUCCESS_BIT);
            xEventGroupSetBits(g_sensors_event, DATA_PHG206A_FAILED_BIT);
        }

        time_t now = 0;
        time(&now);
        g_unix_get_timestamp = (uint32_t)now;
        g_sensor.TimeStamp = g_unix_get_timestamp;

        if(xQueueSend(def->SensorsQueue, &g_sensor, (TickType_t)100)==pdTRUE)
        {
            ESP_LOGI(SENSORS_TASK, "Send data to queue successfully");
        }
        else
        {
            ESP_LOGI(SENSORS_TASK,"Failed to send data to queue");
            if (uxQueueSpacesAvailable(def->SensorsQueue)<5)
            {
                ESP_LOGI(SENSORS_TASK, "Queue is full");
                xQueueReset(def->SensorsQueue);
            }
        }
        xEventGroupClearBits(def->SensorsManager, READY_MEASUREMENT_EVENT);
        ESP_LOGW(SENSORS_TASK, "Finished sensors task!\n");
        xEventGroupSetBits(def->SensorsManager, FINISH_MEASUREMENT_EVENT);
    }
}

typedef void (*command_handler_t)(char *args, CheminsDef *def);

typedef struct {
    const char *command;
    command_handler_t handler;
} command_t;

static void handle_salinity(char *args, CheminsDef *def)                // Command set value for salinity: chemins/salinity: 10
{
    uint16_t salinity = (uint16_t)atof(args);
    if (salinity <= 50)
    {
        ESP_LOGI(ADJUST_TASK, "Set salinity: %u", salinity);
        salinity = (uint16_t) salinity*10;
        rdo206a_set_salinity(UART_NUM, salinity);
    }
    else
    {
        ESP_LOGE(ADJUST_TASK, "Salinity value is out of range! (0 - 50)");
    }
}

static void handle_revolution(char *args, CheminsDef *def)              // Command set value for revolution: chemins/revolution: 5
{
    uint8_t adjust_revolutions = (uint8_t)atoi(args);
    if (adjust_revolutions > 0 && adjust_revolutions <= 10)
    {
        ESP_LOGI(ADJUST_TASK,"Set revolutions: %d", adjust_revolutions);
        cleanup_set_revolutions((uint8_t)adjust_revolutions);
    }
    else
    {
        ESP_LOGE(ADJUST_TASK, "Revolution value is out of range! (1 - 10)");
    }
}

static void handle_run_cleaner(char *args, CheminsDef *def)             // Command run cleaner: chemins/run cleaner: 5        
{
    uint8_t adjust_revolutions = (uint8_t)atoi(args);
    if (adjust_revolutions > 0 && adjust_revolutions <= 10)
    {
        ESP_LOGI(ADJUST_TASK,"Run cleaner: %u turns!\n", adjust_revolutions);
        cleanup_set_revolutions((uint8_t)adjust_revolutions);
        clean_set_auto_revolution(UART_NUM, adjust_revolutions);
    }
    else
    {
        ESP_LOGE(ADJUST_TASK, "Revolution value is out of range! (1 - 10)");
    }
}

static void handle_interval_time(char *args, CheminsDef *def)           // Command set value for interval time: chemins/interval time: 10
{
    uint16_t interval_time = (uint16_t)atof(args);
    if (interval_time >= 6 && interval_time <= 6000)
    {
        ESP_LOGI(ADJUST_TASK, "Set interval time: %u", interval_time);
        clean_set_auto_interval_time(UART_NUM, interval_time);
    }
    else
    {
        ESP_LOGE(ADJUST_TASK, "Interval time is out of range! (6 - 6000)");
    }
}

static void handle_measurement_time(char *args, CheminsDef *def)        // Command set value for measurement time: chemins/measurement time: 60000
{
    g_measurement_time = (uint64_t)atol(args);
    stop_esp_timer();
    ESP_LOGI(ADJUST_TASK, "Set new measurement time: %lld", g_measurement_time);
    start_esp_timer();
}

static void handle_calib_temperature_phg(char *args, CheminsDef *def)   // Command calibrate temperature for PHG206A: chemins/calib temperature phg: 25.5
{
    float temperature_calib = atof(args);
    ESP_LOGI(ADJUST_TASK, "Calibrate temperature PHG206A: %f", temperature_calib);
    temperature_calib = (uint16_t) (temperature_calib * 10);
    phg206a_write_temperature(UART_NUM, (uint16_t)temperature_calib);
}

static void handle_calib_temperature_rdo(char *args, CheminsDef *def)   // Command calibrate temperature for RDO206A: chemins/calib temperature rdo: 25.5
{
    float temperature_calib = atof(args);
    ESP_LOGI(ADJUST_TASK, "Calibrate temperature RDO206A: %f", temperature_calib);
    temperature_calib = (uint16_t) (temperature_calib * 10);
    rdo206a_write_temperature(UART_NUM, (uint16_t)temperature_calib);
}

// Command handler table
static const command_t command_table[] = {
    {"salinity", handle_salinity},                                      // Command set value for salinity: chemins/salinity: 10        
    {"revolution", handle_revolution},                                  // Command set value for revolution: chemins/revolution: 5
    {"run cleaner", handle_run_cleaner},                                // Command run cleaner: chemins/run cleaner: 5
    {"interval time", handle_interval_time},                            // Command set value for interval time: chemins/interval time: 10   
    {"measurement time", handle_measurement_time},                      // Command set value for measurement time: chemins/measurement time: 60000
    {"calib temperature phg", handle_calib_temperature_phg},            // Command calibrate temperature for PHG206A: chemins/calib temperature phg: 25.5
    {"calib temperature rdo", handle_calib_temperature_rdo},            // Command calibrate temperature for RDO206A: chemins/calib temperature rdo: 25.5
};

void adjust_chemins_task(void *arg)
{
    g_adjust_chemins_event = xEventGroupCreate();
    CheminsDef *def = (CheminsDef *)arg;
    BaseType_t ret;
    char command[200];

    while (1) {
        xEventGroupWaitBits(def->CheminsManager, READY_ADJUST_EVENT, pdTRUE, pdTRUE,portMAX_DELAY);
        ESP_LOGW(ADJUST_TASK, "Into adjust task!\n");

        ret = xQueueReceive(def->CheminsValueQueue, command, 5);
        if (ret==pdTRUE)
        {
            ESP_LOGI(ADJUST_TASK, "Receive command data from queue successfully: %s", command);
            char *cmd = strtok(command, ":");
            char *args = strtok(NULL, "");
            ESP_LOGI(ADJUST_TASK,"Into adjust");

            // Search for command table for a matching command
            uint8_t command_found_flag = 0;
            for (size_t i = 0; i < sizeof(command_table) / sizeof(command_table[0]); i++)
            {
                if (strcmp(command_table[i].command, cmd) == 0)
                {
                    command_table[i].handler(args, def);
                    command_found_flag = 1;
                    break;
                }
            }

            if (!command_found_flag)
            {
                ESP_LOGE(ADJUST_TASK, "Unknown command: %s !!!", cmd);
            }
        }
        ESP_LOGW(ADJUST_TASK, "Finished adjust task!\n");  
        xEventGroupSetBits(def->CheminsManager, FINISH_ADJUST_EVENT);
    }
}