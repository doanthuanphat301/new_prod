#ifndef SENSORS_TASK_H
#define SENSORS_TASK_H

typedef struct 
{
    QueueHandle_t SensorsQueue;
    EventGroupHandle_t SensorsManager;
} SensorsDef;

void sensors_task(void *arg);

typedef struct
{
    QueueHandle_t CheminsValueQueue;
    EventGroupHandle_t CheminsManager;
} CheminsDef;

void adjust_chemins_task(void *arg);

#endif // SENSORS_TASK_H