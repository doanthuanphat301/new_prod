#ifndef _COMMON_CHEMINS_H_
#define _COMMON_CHEMINS_H_

#include "global.h"
#include "crc16.h"
#include "register_common_chemins.h"

// typedef struct
// {
//     struct phg206_a_value PghValue;
//     struct rdo206_a_value RdoValue;
// }SensorsData;

/**
 * @brief Structure to hold pH and temperature values along with calibration and device address.
 */
typedef struct
{
  float PH;               /**< pH value */
  float Temp;             /**< Temperature value */
  float ZeroCalib;       /**< Zero calibration value */
  float SlopeCalib;      /**< Slope calibration value */
  uint8_t DeviceAddress; /**< Device address */
} Phg206a;

typedef struct
{
  float DO;              /**< DO value */
  float Temp;            /**< Temperature value */
  float Salinity;        /**< Salinity value */
  float ZeroCalib;       /**< Zero calibration value */
  float SlopeCalib;      /**< Slope calibration value */
  uint8_t DeviceAddress; /**< Device address */
} Rdo206a;

typedef struct
{
  uint8_t DeviceAddress; /**< Device address */
  uint8_t Revolution;     /**< Revolution */
  uint16_t IntervalTime; /**< Interval Time */
} Clean200;


typedef struct
{
  Phg206a Phg206AValue;
  Rdo206a Rdo206AValue;
  Clean200 Clean200;
} CheminsValue;

typedef struct
{
    Phg206a Phg206AValue;
    Rdo206a Rdo206AValue;
    uint32_t TimeStamp;
}SensorsData;
typedef enum
{
  PHG206A_DEVICES = 0,
  CLEAN200_DEVICES = 1,
  RDO206A_DEVICES = 2,
  NONE_DEVICES = 3,
} CurrentDeviceChemins;

typedef enum
{
  eCHEMINS_FAIL = 0,
  eCHEMINS_OK = 1
} CHEMINS_STATE;

/**
 * @brief Enumeration to select the type of value to get from the device.
 */

/**
 * @brief Configures the UART interface.
 * 
 * @param uart_num UART port number
 * @param rx_buffer_size Receive buffer size
 * @param tx_buffer_size Transmit buffer size
 * @param tout_thresh Timeout threshold
 */
void chemins_uart_init(uart_port_t uart_num, int tx_io_num, int rx_io_num, int rx_buffer_size, int tx_buffer_size);
CHEMINS_STATE chemins_uart_read_data(uart_port_t uart_num, uint8_t device_address, uint16_t register_address, uint16_t num_registers);
CHEMINS_STATE chemins_uart_write_data(uart_port_t uart_num, uint8_t device_address, uint16_t register_address, uint16_t num_registers);

#endif // _COMMON_CHEMINS_H_