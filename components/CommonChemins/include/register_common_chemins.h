#ifndef REGISTER_COMMON_CHEMINS_H
#define REGISTER_COMMON_CHEMINS_H

// Define for RDO-206A

// PHG- 206A function code

#define DEVICE_ADDRESS_PHG206A                  0x03    // device address 0x06-> 0x01
#define DEVICE_ADDRESS_RDO206A                  0x05
#define DEVICE_ADDRESS_CLEAN200                 0x24
#define FUNCTION_CODE_READ                      0x03
#define FUNCTION_CODE_WRITE                     0x06

// Define for PHG-206A
#define TIMEOUT_WAIT_RESPONSE_DATA_READ         100
// PHG-206A Calibration Registers
#define REG_ZERO_CALI_PH6_86_PHG206A            0x1000  // Zero calibration for pH 6.86
#define REG_ZERO_CALI_PH6_86_NUM_PHG206A        0x0000
#define REG_SLOPE_CALI_PH4_PHG206A              0x1002  // Slope calibration for pH 4.00
#define REG_SLOPE_CALI_PH4_NUM_PHG206A          0x0000
#define REG_SLOPE_CALI_PH9_18_PHG206A           0x1004  // Slope calibration for pH 9.18
#define REG_SLOPE_CALI_PH9_18_NUM_PHG206A       0x0000
#define REG_ZERO_CALI_VALUE_PHG206A             0x1006  // Read zero calibration value
#define REG_ZERO_CALI_VALUE_NUM_PHG206A         0x0000
#define REG_SLOPE_CALI_VALUE_PHG206A            0x1008  // Read slope calibration value
#define REG_SLOPE_CALI_VALUE_NUM_PHG206A        0x0000
#define REG_TEMPERATURE_VALUE_PHG206A           0x1010  // Write/Read temperature calibration value
#define REG_TEMPERATURE_VALUE_NUM_PHG206A       0x0000
// PHG-206A Registers

// RDO-206A Registers
#define REG_SALINITY_VALUE_RDO_206A             0x1020 // Read/Write Salinity value
#define REG_SWITCH_MACHINE_RDO_206A             0x1100 // Write switch machine (0x0000: turn off, 0x0001: turn on)
#define REG_TEMPERATURE_VALUE_RDO206A           0x1010 // Write/Read temperature calibration value
// RDO-206A Registers
// CLEAN200 Registers
#define REG_AUTO_CLEAN_INTERVAL_TIME            0x1300
#define REG_AUTO_CLEAN_INTERVAL_TIME_NUM        0x0000
#define REG_AUTO_CLEAN_REVOL                    0X1301
#define REG_AUTO_CLEAN_REVOL_NUM                0X0000
// CLEAN200 Registers


// Other Registers
#define REG_MEASURED_TEMPERATURE                0x0000  // the measured value and temperature
#define REG_MEASURED_TEMPERATURE_NUM            0x0004
#define REG_DEVICE_ADDRESS                      0x2002  // Write/Read device address (1-127)
#define REG_DEVICE_ADDRESS_NUM                  0x0000
#define REG_FACTORY_RESET                       0x2020  // Write factory reset (restore calibration values to factory settings)
#define REG_FACTORY_RESET_NUM                   0x0000

#endif // REGISTER_COMMON_CHEMINS_H