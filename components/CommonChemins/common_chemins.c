#include "common_chemins.h"

char *TAG_CHEMINS = "NO_DEVICE";

CheminsValue chemins_value;
CurrentDeviceChemins g_running_device_chemins;
uart_event_t g_event_chemins;
QueueHandle_t g_uart_queue_chemins;
SemaphoreHandle_t xCheminsMutex;
void chemins_uart_init(uart_port_t uart_num, int tx_io_num, int rx_io_num, int rx_buffer_size, int tx_buffer_size)
{
    uint8_t clear_frame[20];
    // UART configuration
    uart_config_t uart_config =
        {
            .baud_rate = BAUD_RATE,
            .data_bits = UART_DATA_8_BITS,
            .parity = UART_PARITY_DISABLE,
            .stop_bits = UART_STOP_BITS_1,
            .flow_ctrl = UART_HW_FLOWCTRL_DISABLE,
            .source_clk = UART_SCLK_DEFAULT,
        };
    uart_driver_install(uart_num, rx_buffer_size, tx_buffer_size, 20, &g_uart_queue_chemins, 0);
    uart_param_config(uart_num, &uart_config);
    // uart_set_rx_timeout(uart_num, tout_thresh);
    uart_set_pin(uart_num, tx_io_num, rx_io_num, UART_PIN_NO_CHANGE, UART_PIN_NO_CHANGE);
    uart_set_mode(uart_num, UART_MODE_RS485_HALF_DUPLEX);
    uart_read_bytes(uart_num, clear_frame, 20, 100 / portTICK_PERIOD_MS);
    xCheminsMutex = xSemaphoreCreateMutex();
    if (!xCheminsMutex)
    {
        ESP_LOGE(TAG_CHEMINS, "Could not create uart mutex");
    }
}

void change_tag_device_chemins(uint8_t device_address)
{
    if (device_address == DEVICE_ADDRESS_PHG206A)
    {
        TAG_CHEMINS = "PHG206A";
    }
    else if (device_address == DEVICE_ADDRESS_RDO206A)
    {
        TAG_CHEMINS = "RDO206A";
    }
    else if (device_address == DEVICE_ADDRESS_CLEAN200)
    {
        TAG_CHEMINS = "CLEAN200";
    }
}
void calculate_value_response(uint8_t *response_frame, CurrentDeviceChemins current_device)
{

    uint16_t pH_DO_raw = (response_frame[3] << 8) | response_frame[4];
    uint16_t pH_DO_decimals = response_frame[6];
    uint16_t temperature_raw = (response_frame[7] << 8) | response_frame[8];
    uint16_t temperature_decimals = response_frame[10];
    if (current_device == PHG206A_DEVICES)
    {
        chemins_value.Phg206AValue.PH = pH_DO_raw / pow(10, pH_DO_decimals);
        chemins_value.Phg206AValue.Temp = temperature_raw / pow(10, temperature_decimals);
        ESP_LOGI(TAG_CHEMINS, "--> pH: %.2f, Temperature_PHG: %.1f °C \n", chemins_value.Phg206AValue.PH, chemins_value.Phg206AValue.Temp);
    }
    else if (current_device == RDO206A_DEVICES)
    {
        chemins_value.Rdo206AValue.DO = pH_DO_raw / pow(10, pH_DO_decimals);
        chemins_value.Rdo206AValue.Temp = temperature_raw / pow(10, temperature_decimals);
        ESP_LOGI(TAG_CHEMINS, "--> Dissolved oxygen: %.2f mg/L, Temperature_RDO: %.1f °C \n", chemins_value.Rdo206AValue.DO, chemins_value.Rdo206AValue.Temp);
    }
}
void check_current_device(uint8_t current_device)
{
    change_tag_device_chemins(current_device);
    if (current_device == DEVICE_ADDRESS_PHG206A)
    {
        ESP_LOGE(TAG_CHEMINS, "Current device is PHG206A");
        g_running_device_chemins = PHG206A_DEVICES;
    }
    else if (current_device == DEVICE_ADDRESS_RDO206A)
    {
        ESP_LOGE(TAG_CHEMINS, "Current device is RDO206A");
        g_running_device_chemins = RDO206A_DEVICES;
    }
    else if (current_device == DEVICE_ADDRESS_CLEAN200)
    {
        ESP_LOGE(TAG_CHEMINS, "Current device is CLEAN200");
        g_running_device_chemins = CLEAN200_DEVICES;
    }
    else
    {
        ESP_LOGE(TAG_CHEMINS, "Current device is unknown");
        g_running_device_chemins = NONE_DEVICES;
    }
}
CHEMINS_STATE check_response_frame(uart_port_t uart_num, uint8_t *request_frame, uint8_t devices_address)
{
    // ESP_LOGI(TAG, "pass uart read response");
    // uint8_t device_address = request_frame[0];
    uint8_t function_code_request = request_frame[1];
    uint8_t device_address_request = request_frame[0];
    int length_rx = 0;
    uint8_t response_frame[20];
    uint8_t number_data = 0;
    uint16_t crc_check;
    bool check_response_flag = false;
    TickType_t start_time = xTaskGetTickCount();
    TickType_t timeout_ticks = pdMS_TO_TICKS(TIMEOUT_WAIT_RESPONSE_DATA_READ);

    while ((xTaskGetTickCount() - start_time) < timeout_ticks)
    {
        if (xQueueReceive(g_uart_queue_chemins, (void *)&g_event_chemins, (TickType_t)2))
        {
            bzero(response_frame, 20);
            if (g_event_chemins.type == UART_DATA)
            {
                ESP_LOGI(TAG_CHEMINS, "pass UART DATA event");
                length_rx = uart_read_bytes(uart_num, response_frame, g_event_chemins.size, 100 / portTICK_PERIOD_MS);
                break;
            }
        }
    }

    // if (xQueueReceive(g_uart_queue_chemins, (void *)&g_event_chemins, TIMEOUT_WAIT_RESPONSE_DATA_READ/portTICK_PERIOD_MS)) {
    //     bzero(response_frame, 20);
    //     if(g_event_chemins.type == UART_DATA){
    //         ESP_LOGI(TAG_CHEMINS, "pass UART DATA event");
    //         length_rx = uart_read_bytes(uart_num,response_frame, g_event_chemins.size, 100/portTICK_PERIOD_MS);
    //     }
    // }

    ESP_LOGI(TAG_CHEMINS, "Length of Received response from sensor: %d \n", length_rx);
    char log_buffer[256] = {0};
    int log_buffer_pos = 0; // Track the current position in log_buffer
    for (int i = 0; i < length_rx; i++)
    {
        // Calculate remaining buffer size, ensuring space for null terminator
        int remaining_size = sizeof(log_buffer) - log_buffer_pos - 1;
        if (remaining_size > 0)
        {
            // Use snprintf to safely append to log_buffer without overflow
            int written = snprintf(log_buffer + log_buffer_pos, remaining_size, "0x%02X ", response_frame[i]);
            if (written > 0)
            {
                log_buffer_pos += written; // Update position by the number of written characters
            }
        }
    }
    ESP_LOGI(TAG_CHEMINS, "Received data from sensor: %s", log_buffer);
    if (length_rx < 8)
    {
        ESP_LOGE(TAG_CHEMINS, "Error reading\n");
        return eCHEMINS_FAIL;
    }
    if (g_running_device_chemins != NONE_DEVICES)
    {
        uint8_t function_code_response = response_frame[1];
        uint8_t device_address_response = response_frame[0];

        if ((g_running_device_chemins == PHG206A_DEVICES && device_address_response == DEVICE_ADDRESS_PHG206A && device_address_request == DEVICE_ADDRESS_PHG206A) ||
            (g_running_device_chemins == RDO206A_DEVICES && device_address_response == DEVICE_ADDRESS_RDO206A && device_address_request == DEVICE_ADDRESS_RDO206A) ||
            (g_running_device_chemins == CLEAN200_DEVICES && device_address_response == DEVICE_ADDRESS_CLEAN200 && device_address_request == DEVICE_ADDRESS_CLEAN200))
            if (function_code_request == FUNCTION_CODE_READ && function_code_response == FUNCTION_CODE_READ)
            {
                number_data = response_frame[2];
                crc_check = calculate_crc(response_frame, (g_event_chemins.size - 2));
                if ((response_frame[g_event_chemins.size - 2] == (crc_check & 0xFF)) && (response_frame[g_event_chemins.size - 1] == ((crc_check >> 8) & 0xFF)))
                {
                    ESP_LOGI(TAG_CHEMINS, "pass uart crc check");
                    if (number_data == 8)
                    {

                        calculate_value_response(response_frame, g_running_device_chemins);
                    }
                    else if (number_data == 2)
                    {
                        switch (g_running_device_chemins)
                        {
                        case PHG206A_DEVICES:
                            ESP_LOGI(TAG_CHEMINS, "converting data on device");
                            switch ((request_frame[2] << 8 | request_frame[3]))
                            {
                            case REG_TEMPERATURE_VALUE_PHG206A:
                                chemins_value.Phg206AValue.Temp = (response_frame[3] << 8 | response_frame[4]) * 10;
                                break;
                            case REG_SLOPE_CALI_VALUE_PHG206A:
                                chemins_value.Phg206AValue.SlopeCalib = (response_frame[3] << 8 | response_frame[4]) / 1000;
                                break;
                            case REG_ZERO_CALI_VALUE_PHG206A:
                                chemins_value.Phg206AValue.ZeroCalib = (response_frame[3] << 8 | response_frame[4]);
                                break;
                            case REG_DEVICE_ADDRESS:
                                chemins_value.Phg206AValue.DeviceAddress = (response_frame[3] << 8 | response_frame[4]);
                                break;

                            default:
                                break;
                            }
                            break;
                        case RDO206A_DEVICES:
                            ESP_LOGI(TAG_CHEMINS, "converting data on device");
                            switch ((request_frame[2] << 8 | request_frame[3]))
                            {
                            case REG_TEMPERATURE_VALUE_PHG206A:
                                chemins_value.Rdo206AValue.Temp = (response_frame[3] << 8 | response_frame[4]) * 10;
                                break;
                            case REG_SLOPE_CALI_VALUE_PHG206A:
                                chemins_value.Rdo206AValue.SlopeCalib = (response_frame[3] << 8 | response_frame[4]) / 1000;
                                break;
                            case REG_ZERO_CALI_VALUE_PHG206A:
                                chemins_value.Rdo206AValue.ZeroCalib = (response_frame[3] << 8 | response_frame[4]);
                                break;
                            case REG_SALINITY_VALUE_RDO_206A:
                                chemins_value.Rdo206AValue.Salinity = (response_frame[3] << 8 | response_frame[4]);
                                break;
                            case REG_DEVICE_ADDRESS:
                                chemins_value.Rdo206AValue.DeviceAddress = (response_frame[3] << 8 | response_frame[4]);
                                break;

                            default:
                                break;
                            }
                            break;
                        case CLEAN200_DEVICES:
                            ESP_LOGI(TAG_CHEMINS, "converting data on device");
                            switch ((request_frame[2] << 8 | request_frame[3]))
                            {
                            case REG_AUTO_CLEAN_INTERVAL_TIME:
                                chemins_value.Clean200.IntervalTime = (response_frame[3] << 8 | response_frame[4]);
                                ESP_LOGI(TAG_CHEMINS, "clean_interval_time value: %d", (int)chemins_value.Clean200.IntervalTime);
                                break;
                            case REG_AUTO_CLEAN_REVOL:
                                chemins_value.Clean200.Revolution = (response_frame[3] << 8 | response_frame[4]);
                                break;
                            default:
                                break;
                            }
                            break;
                        default:
                            break;
                        }
                    }
                    check_response_flag = eCHEMINS_OK;
                }
            }
        if (function_code_request == FUNCTION_CODE_WRITE && function_code_response == FUNCTION_CODE_WRITE)
        {
            uint8_t length = g_event_chemins.size;
            if (length == 8)
            {
                if (response_frame[0] == request_frame[0] &&
                    response_frame[1] == request_frame[1] &&
                    response_frame[2] == request_frame[2] &&
                    response_frame[3] == request_frame[3] &&
                    response_frame[4] == request_frame[4] &&
                    response_frame[5] == request_frame[5] &&
                    response_frame[6] == request_frame[6] &&
                    response_frame[7] == request_frame[7])
                {
                    ESP_LOGI(TAG_CHEMINS, "Write data successfully!\n");
                    check_response_flag = eCHEMINS_OK;
                }
            }
        }
        if (function_code_request == response_frame[1] + 0x80 && function_code_response == response_frame[1] + 0x80)
        {
            crc_check = calculate_crc((uint8_t *)response_frame, (g_event_chemins.size - 2));
            if ((response_frame[g_event_chemins.size - 2] == (crc_check & 0xFF)) && (response_frame[g_event_chemins.size - 1] == ((crc_check >> 8) & 0xFF)))
            {
                switch (response_frame[2])
                {
                case 0x01:
                    ESP_LOGE(TAG_CHEMINS, "Function code error");
                    break;
                case 0x03:
                    ESP_LOGE(TAG_CHEMINS, "Data is wrong");
                    break;
                default:
                    ESP_LOGE(TAG_CHEMINS, "No dectect error");
                    break;
                }
                check_response_flag = eCHEMINS_OK;
            }
        }
    }
    else
    {
        ESP_LOGE(TAG_CHEMINS, "Error Reading\n");
    }

    if (check_response_flag == eCHEMINS_OK)
    {
        return eCHEMINS_OK;
    }
    else
    {
        return eCHEMINS_FAIL;
    }
}

CHEMINS_STATE chemins_read(uart_port_t uart_num, uint8_t device_address, uint16_t register_address, uint16_t num_registers)
{
    CHEMINS_STATE check_response_flags = eCHEMINS_FAIL;
    uint8_t request_frame[8];
    request_frame[0] = device_address;
    request_frame[1] = FUNCTION_CODE_READ;
    request_frame[2] = (register_address >> 8) & 0xFF; // Register address (high byte)
    request_frame[3] = register_address & 0xFF;        // Register address (low byte)
    request_frame[4] = (num_registers >> 8) & 0xFF;    // Number of registers (high byte)
    request_frame[5] = num_registers & 0xFF;           // Number of registers (low byte)

    // Calculate CRC for request frame
    uint16_t crc = calculate_crc(request_frame, 6);
    request_frame[6] = crc & 0xFF;        // CRC (low byte)
    request_frame[7] = (crc >> 8) & 0xFF; // CRC (high byte)
    if (xSemaphoreTake(xCheminsMutex, 1000 / portTICK_PERIOD_MS))
    {
        // Send request frame via UART

        check_current_device(device_address);
        ESP_LOGI(TAG_CHEMINS, "Read data from sensor: 0x%02X 0x%02X 0x%02X 0x%02X 0x%02X 0x%02X 0x%02X 0x%02X\n", request_frame[0],
                 request_frame[1], request_frame[2], request_frame[3], request_frame[4], request_frame[5], request_frame[6], request_frame[7]);
        uart_write_bytes(uart_num, request_frame, 8);
        check_response_flags = check_response_frame(uart_num, request_frame, device_address);
    }
    xSemaphoreGive(xCheminsMutex);
    if (check_response_flags == eCHEMINS_OK)
    {
        return eCHEMINS_OK;
    }
    else
    {
        return eCHEMINS_FAIL;
    }
}

CHEMINS_STATE chemins_uart_read_data(uart_port_t uart_num, uint8_t device_address, uint16_t register_address, uint16_t num_registers)
{

    if (chemins_read(uart_num, device_address, register_address, num_registers) == eCHEMINS_OK)
    {
        ESP_LOGI(TAG_CHEMINS, "Read data chemins successfully!");
        return eCHEMINS_OK;
    }

    // Retry read data if failed
    const uint8_t MAX_RETRY_READ = 3;
    for (uint8_t retry_read = 0; retry_read < MAX_RETRY_READ; retry_read++)
    {
        ESP_LOGW(TAG_CHEMINS, "Read data failed, retrying (%d/%d)...", retry_read + 1, MAX_RETRY_READ);
        vTaskDelay(1000 / portTICK_PERIOD_MS); // Delay 1s between retries
        if (chemins_read(uart_num, device_address, register_address, num_registers) == eCHEMINS_OK)
        {
            ESP_LOGI(TAG_CHEMINS, "Read data chemins successfully");
            return eCHEMINS_OK;
        }
    }

    // If all retries failed
    ESP_LOGE(TAG_CHEMINS, "Read data failed after %d attempts, check device address and connection", MAX_RETRY_READ);
    return eCHEMINS_FAIL;
}
CHEMINS_STATE chemins_write(uart_port_t uart_num, uint8_t device_address, uint16_t register_address, uint16_t num_registers)
{
    CHEMINS_STATE check_response_flags = eCHEMINS_FAIL;
    // Create data read request
    uint8_t request_frame[8];
    request_frame[0] = device_address;
    request_frame[1] = FUNCTION_CODE_WRITE;
    request_frame[2] = (register_address >> 8) & 0xFF; // Register address (high byte)
    request_frame[3] = register_address & 0xFF;        // Register address (low byte)
    request_frame[4] = (num_registers >> 8) & 0xFF;    // Number of registers (high byte)
    request_frame[5] = num_registers & 0xFF;           // Number of registers (low byte)

    // Calculate CRC for request frame
    uint16_t crc = calculate_crc(request_frame, 6);
    request_frame[6] = crc & 0xFF;        // CRC (low byte)
    request_frame[7] = (crc >> 8) & 0xFF; // CRC (high byte)
    if (xSemaphoreTake(xCheminsMutex, 1000 / portTICK_PERIOD_MS))
    {
        // Send request frame via UART

        check_current_device(device_address);
        ESP_LOGI(TAG_CHEMINS, "Write data to sensor: 0x%02X 0x%02X 0x%02X 0x%02X 0x%02X 0x%02X 0x%02X 0x%02X\n", request_frame[0],
                 request_frame[1], request_frame[2], request_frame[3], request_frame[4], request_frame[5], request_frame[6], request_frame[7]);

        uart_write_bytes(uart_num, request_frame, 8);
        check_response_flags = check_response_frame(uart_num, request_frame, device_address);
    }
    xSemaphoreGive(xCheminsMutex);
    if (check_response_flags == eCHEMINS_OK)
    {
        return eCHEMINS_OK;
    }
    else
    {
        return eCHEMINS_FAIL;
    }
}

CHEMINS_STATE chemins_uart_write_data(uart_port_t uart_num, uint8_t device_address, uint16_t register_address, uint16_t num_registers)
{

    if (chemins_write(uart_num, device_address, register_address, num_registers) == eCHEMINS_OK)
    {
        ESP_LOGI(TAG_CHEMINS, "Write data chemins successfully");
        return eCHEMINS_OK;
    }

    // Retry write data if failed
    const uint8_t MAX_RETRY_WRITE = 3;
    for (uint8_t retry_write = 0; retry_write < MAX_RETRY_WRITE; retry_write++)
    {
        ESP_LOGW(TAG_CHEMINS, "Write data failed, retrying (%d/%d)...", retry_write + 1, MAX_RETRY_WRITE);
        vTaskDelay(1000 / portTICK_PERIOD_MS); // Delay 1s between retries
        if (chemins_write(uart_num, device_address, register_address, num_registers) == eCHEMINS_OK)
        {
            ESP_LOGI(TAG_CHEMINS, "Write data chemins successfully");
            return eCHEMINS_OK;
        }
    }

    // If all retries failed
    ESP_LOGE(TAG_CHEMINS, "Write data failed after %d attempts, check device address and connection", MAX_RETRY_WRITE);
    return eCHEMINS_FAIL;
}