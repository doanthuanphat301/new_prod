#include "ota.h"
const char *OTA_TAG = "HTTPS_OTA";
static char g_fw_link[100];
static bool g_newfirmware_exist =false;
 static esp_http_client_config_t g_client_ota_config =
{
    .url = g_fw_link,
    .timeout_ms = 5000,
    .keep_alive_enable = true,
    .crt_bundle_attach = esp_crt_bundle_attach,
}; //CONFIG
// int version_number=1; //Just for testing
// CheckVersion g_check_ptr;
// char g_http_response_buffer[BUFFER_OTA_SIZE];
// int g_http_response_buffer_index = 0;
extern esp_mqtt_client_handle_t g_mqtt_client;
static TaskHandle_t g_ota_handler;
static TaskHandle_t g_list_suspend_task[MAX_LIST];
static uint8_t g_numbers_of_suspend_tasks=0;
static OtaConfig ota_task_config;
void https_ota_event_handler(void* arg, esp_event_base_t event_base,
                          int32_t event_id, void* event_data)
{
    if (event_base == ESP_HTTPS_OTA_EVENT)
    {
        switch (event_id) {
            case ESP_HTTPS_OTA_START:
                ESP_LOGI(OTA_TAG, "OTA started");
                break;
            case ESP_HTTPS_OTA_CONNECTED:
                ESP_LOGI(OTA_TAG, "Connected to server");
                break;
            case ESP_HTTPS_OTA_GET_IMG_DESC:
                ESP_LOGI(OTA_TAG, "Reading Image Description");
                break;
            case ESP_HTTPS_OTA_VERIFY_CHIP_ID:
                ESP_LOGI(OTA_TAG, "Verifying chip id of new image: %d", *(esp_chip_id_t *)event_data);
                break;
            case ESP_HTTPS_OTA_DECRYPT_CB:
                ESP_LOGI(OTA_TAG, "Callback to decrypt function");
                break;
            case ESP_HTTPS_OTA_WRITE_FLASH:
                ESP_LOGD(OTA_TAG, "Writing to flash: %d written", *(int *)event_data);
                break;
            case ESP_HTTPS_OTA_UPDATE_BOOT_PARTITION:
                ESP_LOGI(OTA_TAG, "Boot partition updated. Next Partition: %d", *(esp_partition_subtype_t *)event_data);
                break;
            case ESP_HTTPS_OTA_FINISH:
                ESP_LOGI(OTA_TAG, "OTA finish");
                break;
            case ESP_HTTPS_OTA_ABORT:
                ESP_LOGI(OTA_TAG, "OTA abort");
                break;
        }
    }
}
esp_err_t check_header_image(esp_app_desc_t *new_app_info)
{
    if (new_app_info == NULL)
    {
        return ESP_ERR_INVALID_ARG;
    }

    const esp_partition_t *running = esp_ota_get_running_partition();
    esp_app_desc_t running_app_info;
    if (esp_ota_get_partition_description(running, &running_app_info) == ESP_OK)
    {
        ESP_LOGI(OTA_TAG, "Running firmware version: %s", running_app_info.version);
    }
    ESP_LOGI(OTA_TAG, "New firmware version: %s", new_app_info->version);

#ifndef CONFIG_EXAMPLE_SKIP_VERSION_CHECK
    if(strcmp(new_app_info->version,running_app_info.version)<=0)
    {
        ESP_LOGW(OTA_TAG, "It's a lower version. We will not continue the update.");
        return ESP_FAIL;
    }
#endif
    return ESP_OK;
}

OTA_RESULT ota_perform(void *pvParameter)
{
    // EventGroupHandle_t event_manager_t = (EventGroupHandle_t)pvParameter;
    ESP_LOGI(OTA_TAG, "Starting  OTA:");
    esp_err_t ota_finish_err = ESP_OK;
#ifdef CONFIG_SKIP_COMMON_NAME_CHECK
    config.skip_cert_common_name_check = true;
#endif

    esp_https_ota_config_t ota_config =
    {
        .http_config = &g_client_ota_config,
#ifdef CONFIG_ENABLE_PARTIAL_HTTP_DOWNLOAD
        .partial_http_download = true,
        .max_http_request_size = CONFIG_EXAMPLE_HTTP_REQUEST_SIZE,
#endif
    };
    esp_https_ota_handle_t https_ota_handle = NULL;
    esp_err_t err = esp_https_ota_begin(&ota_config, &https_ota_handle);
    if (err != ESP_OK)
    {
        ESP_LOGE(OTA_TAG, "ESP HTTPS OTA Begin failed");
        // if(error_func!=NULL)
        // {
        //     error_func(eOTA_BEGIN_FAILED,(int*)&err);
        // }
        esp_https_ota_abort(https_ota_handle);
        return eOTA_BEGIN_FAILED;
    }

    esp_app_desc_t app_desc;
    err = esp_https_ota_get_img_desc(https_ota_handle, &app_desc);
    if (err != ESP_OK)
    {
        ESP_LOGE(OTA_TAG, "esp_https_ota_read_img_desc failed");
        esp_https_ota_abort(https_ota_handle);
        return eOTA_VERIFICATION_FAILED;
    }
    err = check_header_image(&app_desc);
    if (err != ESP_OK)
    {
        ESP_LOGE(OTA_TAG, "image header verification failed");
        g_newfirmware_exist =false;
        // if(error_func!=NULL)
        // {
        //     error_func(eVERIFICATION_FAILED,(int*)&err);
        // }
        esp_https_ota_abort(https_ota_handle);
        return eOTA_VERIFICATION_FAILED;
    }

    while (1) {
        err = esp_https_ota_perform(https_ota_handle);
        if (err != ESP_ERR_HTTPS_OTA_IN_PROGRESS)
        {
            break;
        }
        // esp_https_ota_perform returns after every read operation which gives user the ability to
        // monitor the status of OTA upgrade by calling esp_https_ota_get_image_len_read, which gives length of image
        // data read so far.
        ESP_LOGW(OTA_TAG, "Image bytes read: %d", esp_https_ota_get_image_len_read(https_ota_handle));
    }

    if (esp_https_ota_is_complete_data_received(https_ota_handle) != true)
    {
        // the OTA image was not completely received and user can customise the response to this situation.
        ESP_LOGE(OTA_TAG, "Complete data was not received.");
        return eOTA_INCOMPLETE_DATA;
    }
    else
    {
        ota_finish_err = esp_https_ota_finish(https_ota_handle);
        if ((err == ESP_OK) && (ota_finish_err == ESP_OK))
        {
            ESP_LOGI(OTA_TAG, "ESP_HTTPS_OTA upgrade successful. Rebooting ...");
            vTaskDelay(5000 / portTICK_PERIOD_MS);
            esp_restart();
        }
        else
        {
            if (ota_finish_err == ESP_ERR_OTA_VALIDATE_FAILED)
            {
                ESP_LOGE(OTA_TAG, "Image validation failed, image is corrupted");
            }
            ESP_LOGE(OTA_TAG, "ESP_HTTPS_OTA upgrade failed 0x%x", ota_finish_err);
            return eOTA_UPDATEFAILED;
        }
    }

// ota_end:
//     esp_https_ota_abort(https_ota_handle);
//     ESP_LOGE(OTA_TAG, "ESP_HTTPS_OTA upgrade failed");
//     xEventGroupClearBits(event_manager_t, READY_OTA_EVENT);
//     xEventGroupSetBits(event_manager_t, OTA_FAILED_EVENT);
//     return;
}

void ota_init(EventGroupHandle_t event_manager,QueueHandle_t ota_queue)
{
    ota_task_config.EventManager = event_manager;
    ota_task_config.RecvQueue = ota_queue;
    ESP_LOGI(OTA_TAG, "OTA app_main start");
    ESP_ERROR_CHECK(esp_event_handler_register(ESP_HTTPS_OTA_EVENT, ESP_EVENT_ANY_ID, &https_ota_event_handler, NULL));
#if CONFIG_CONNECT_WIFI
#if !CONFIG_BT_ENABLED
    /* Ensure to disable any WiFi power save mode, this allows best speed of data transform
     * and reducing download time of OTA
     */
    esp_wifi_set_ps(WIFI_PS_NONE);
#else
    /* WIFI_PS_MIN_MODEM is the default mode for WiFi Power saving. When both
     * WiFi and Bluetooth are running, WiFI modem has to go down, hence we
     * need WIFI_PS_MIN_MODEM. And as WiFi modem goes down, OTA download time
     * increases.
     */
    esp_wifi_set_ps(WIFI_PS_MIN_MODEM);
#endif // CONFIG_BT_ENABLED
#endif // CONFIG_EXAMPLE_CONNECT_WIFI

}
esp_err_t confirm_app(void)
{
    const esp_partition_t *running = esp_ota_get_running_partition();
    esp_ota_img_states_t ota_state;
    if (esp_ota_get_state_partition(running, &ota_state) == ESP_OK) {
        if (ota_state == ESP_OTA_IMG_PENDING_VERIFY) {
            if (esp_ota_mark_app_valid_cancel_rollback() == ESP_OK) {
                ESP_LOGI(OTA_TAG, "App is valid, rollback cancelled successfully");
                    return ESP_OK;
            } else {
                ESP_LOGE(OTA_TAG, "Failed to cancel rollback");
                return ESP_FAIL;
            }
        }
    }
    return ESP_FAIL;
}
esp_err_t cancel_app(void)
{
    esp_err_t err = esp_ota_check_rollback_is_possible();
    if (err != ESP_OK) {
        ESP_LOGE(OTA_TAG, "Rollback is not possible: %s", esp_err_to_name(err));
        return err;
    }
    else
    {
        esp_ota_mark_app_invalid_rollback_and_reboot();
        return ESP_OK;
    }
}
void check_version_task(void *arg)
{
    const esp_partition_t *running = esp_ota_get_running_partition();
    esp_app_desc_t running_app_info;
    BaseType_t ret;
    OtaConfig* config = (OtaConfig*) arg;
    char command[200];
    while(1)
    {
        ret=xQueueReceive(config->RecvQueue,command,(TickType_t)portMAX_DELAY);
        if(ret ==pdTRUE)
        {
            ESP_LOGI(OTA_TAG, "Received command: %s", command);
            if(strcmp(command,"version_check")==0)
            {
                cJSON *json_data;
                json_data = cJSON_CreateObject();
                if (esp_ota_get_partition_description(running, &running_app_info) == ESP_OK)
                {
                    ESP_LOGI(OTA_TAG, "Running firmware version: %s", running_app_info.version);
                }
                cJSON_AddStringToObject(json_data, "deviceCurrentFwVer", running_app_info.version);
                char *my_json_string = cJSON_Print(json_data);
                printf("Send version to ThingsBoards");
                esp_mqtt_client_publish(g_mqtt_client, "v1/devices/me/attributes", my_json_string, strlen(my_json_string), 2, 0);
                cJSON_free(my_json_string);
                cJSON_Delete(json_data);
            }
            else
            {
                char *fw = strtok(command,":");
                fw = strtok(NULL,"");
                printf("link: %s",fw);
                strcpy(g_fw_link,fw);
                g_newfirmware_exist=true;
                // xTaskNotifyGive(g_ota_handler);
                // xEventGroupSetBits(config->EventManager,READY_OTA_EVENT);
            }
        }
    }
}
void add_ota_suspend_task(TaskHandle_t task_handle)
{
    g_list_suspend_task[g_numbers_of_suspend_tasks] = task_handle;
    g_numbers_of_suspend_tasks++;
}
void ota_task(void *arg)
{
    EventGroupHandle_t event_manager = (EventGroupHandle_t) arg;
    uint8_t retry_number =0;
    while(1)
    {
        // ulTaskNotifyTake(pdTRUE,(TickType_t)portMAX_DELAY);
        EventBits_t event_bits=xEventGroupWaitBits(event_manager,START_OTA_EVENT, pdTRUE, pdTRUE, portMAX_DELAY);
        if(checkwifi_state()==eWIFI_CONNECTED)
        {
            retry_number =0;
            printf("g_fw_link : %s\n",g_fw_link);
            ESP_LOGW(OTA_TAG,"into OTA MODE");
            while(ota_perform(event_manager)!=eOTA_VERIFICATION_FAILED && retry_number <MAX_RETRY)
            {
                ESP_LOGE(OTA_TAG,"Something went wrong. Retrying after 5 seconds");
                retry_number++;
                ESP_LOGI(OTA_TAG,"Waiting for wifi checking.....");
                event_bits=xEventGroupWaitBits(event_manager,WIFI_CONNECTED_EVENT, pdFALSE, pdTRUE, (TickType_t)1000/ portTICK_PERIOD_MS);
                if(event_bits & WIFI_CONNECTED_EVENT)
                {
                    ESP_LOGI(OTA_TAG,"Wifi ready for OTA");
                }
                else
                {
                    ESP_LOGW(OTA_TAG,"Wifi not available");
                }
                vTaskDelay(2000 / portTICK_PERIOD_MS);
            }
            ESP_LOGE(OTA_TAG,"OTA failed. Send firmware again.........");
            xEventGroupSetBits(event_manager,READY_MEASUREMENT_EVENT);
        }
        else
        {
            ESP_LOGW(OTA_TAG,"Wifi not available. Can't processing OTA performing.........");
            xEventGroupSetBits(event_manager,READY_MEASUREMENT_EVENT);
        }
    }
}

void suspend_ota_task(void)
{
    vTaskSuspend(g_ota_handler);
}
void resume_ota_task(void)
{
    vTaskResume(g_ota_handler);
}
eTaskState get_ota_task_state(void)
{
    return eTaskGetState(g_ota_handler);
}
void start_ota_task(UBaseType_t priority)
{
    xTaskCreate(&check_version_task, "check_version_task", 1024*5, &ota_task_config,priority , NULL);
    xTaskCreate(&ota_task, "ota_task", 8000, ota_task_config.EventManager, priority, &g_ota_handler);
}

bool check_newfirmware_exist(void)
{
    return g_newfirmware_exist;
}