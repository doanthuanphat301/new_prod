#ifndef SENSOR_RDO206A_H
#define SENSOR_RDO206A_H

#include "global.h"
#include "crc16.h"
#include "common_chemins.h"
#include "register_common_chemins.h"

// struct rdo206_a_value {
//   float DO;               /**< pH value */
//   float temp;             /**< Temperature value */
//   float zero_calib;         /**< Zero calibration value */
//   float slope_calib;        /**< Slope calibration value */
//   uint8_t device_address;   /**< Device address */
// };

typedef enum  {
    RDO206A_DO_e = 0,
    RDO206A_TEMP_e = 1,
    RDO206A_ZERO_CALIB_e = 2,
    RDO206A_SLOPE_CALIB_e = 3,
    RDO206A_SANILITY_e = 4,
    RDO206A_DEVICE_ADDRESS_e = 5
} rdo206a_value_t;

void rdo206a_get_data(rdo206a_value_t select_value_get, Rdo206a *result);
CHEMINS_STATE rdo206a_read_measurement(uart_port_t uart_num);
float rdo206a_read_temperature(uart_port_t uart_num);
CHEMINS_STATE rdo206a_reset_factory(uart_port_t uart_num);
CHEMINS_STATE rdo206a_write_device_address(uart_port_t uart_num, uint16_t num_registers);
uint8_t rdo206a_read_device_address(uart_port_t uart_num);
CHEMINS_STATE rdo206a_set_salinity(uart_port_t uart_num, uint16_t salinity_value);
CHEMINS_STATE rdo206a_write_temperature(uart_port_t uart_num , uint16_t num_registers);
CHEMINS_STATE rdo206a_turn_on(uart_port_t uart_num);
CHEMINS_STATE rdo206a_turn_off(uart_port_t uart_num);

#endif // SENSOR_RDO206A_H