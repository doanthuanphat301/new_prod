#include "sensor_rdo206a.h"


extern CheminsValue chemins_value;
static const char *TAG_RDO206A = "RDO-206A";

void rdo206a_get_data(rdo206a_value_t select_value_get, Rdo206a *result)
{
    switch (select_value_get) {
        case RDO206A_DO_e:
            result->DO = chemins_value.Rdo206AValue.DO;
            break;
        case RDO206A_TEMP_e:
            result->Temp = chemins_value.Rdo206AValue.Temp;
            break;
        case RDO206A_ZERO_CALIB_e:
            result->ZeroCalib = chemins_value.Rdo206AValue.ZeroCalib;
            break;
        case RDO206A_SLOPE_CALIB_e:
            result->SlopeCalib = chemins_value.Rdo206AValue.SlopeCalib;
            break;
        case RDO206A_SANILITY_e:
            result->Salinity = chemins_value.Rdo206AValue.Salinity;
            break;
        case RDO206A_DEVICE_ADDRESS_e:
            result->DeviceAddress = chemins_value.Rdo206AValue.DeviceAddress;
            break;
        default:
            ESP_LOGE(TAG_RDO206A, "Invalid select_value_get");
            break;
    }
}  

CHEMINS_STATE rdo206a_read_measurement(uart_port_t uart_num)
{
    if(chemins_uart_read_data(uart_num, DEVICE_ADDRESS_RDO206A, REG_MEASURED_TEMPERATURE, REG_MEASURED_TEMPERATURE_NUM)){
        ESP_LOGI(TAG_RDO206A, "Measurement read successfully");
        return eCHEMINS_OK;
    }
    else{
        ESP_LOGE(TAG_RDO206A, "Failed to read measurement");
        return eCHEMINS_FAIL;
    }
}

CHEMINS_STATE rdo206a_reset_factory(uart_port_t uart_num)
{
    if(chemins_uart_write_data(uart_num, DEVICE_ADDRESS_RDO206A, REG_FACTORY_RESET, REG_FACTORY_RESET_NUM))
    {
        ESP_LOGI(TAG_RDO206A, "Factory reset successful");
        return eCHEMINS_OK;
    }
    else{
        ESP_LOGE(TAG_RDO206A, "Factory reset failed");
        return eCHEMINS_FAIL;
    }
}


uint8_t rdo206a_read_device_address(uart_port_t uart_num)
{
    if(chemins_uart_read_data(uart_num, DEVICE_ADDRESS_RDO206A, REG_DEVICE_ADDRESS, REG_DEVICE_ADDRESS_NUM)){
        ESP_LOGI(TAG_RDO206A, "Device address read successfully");
        return chemins_value.Rdo206AValue.DeviceAddress;
    }
    else{
        ESP_LOGE(TAG_RDO206A, "Failed to read device address");
        return 0;
    }
}

CHEMINS_STATE rdo206a_write_device_address(uart_port_t uart_num, uint16_t num_registers)
{
    if(chemins_uart_write_data(uart_num, DEVICE_ADDRESS_RDO206A, REG_DEVICE_ADDRESS, num_registers))
    {
        ESP_LOGI(TAG_RDO206A, "Device address [%d] written successfully", num_registers);
        return eCHEMINS_OK;
    }
    else
    {
        ESP_LOGE(TAG_RDO206A, "Failed to write[%d] device address", num_registers);
        return eCHEMINS_FAIL;
    }
}

CHEMINS_STATE rdo206a_set_salinity(uart_port_t uart_num, uint16_t salinity_value)
{
    if(chemins_uart_write_data(uart_num, DEVICE_ADDRESS_RDO206A, REG_SALINITY_VALUE_RDO_206A, salinity_value))
    {
        ESP_LOGI(TAG_RDO206A, "salinity [%d] written successfully", salinity_value);
        return eCHEMINS_OK;
    }
    else
    {
        ESP_LOGE(TAG_RDO206A, "Failed to write [%d] salinity value", salinity_value);
        return eCHEMINS_FAIL;
    }
}

uint16_t rdo206a_get_salinity(uart_port_t uart_num)
{
    if(chemins_uart_read_data(uart_num, DEVICE_ADDRESS_RDO206A, REG_SALINITY_VALUE_RDO_206A, 0))
    {
        ESP_LOGI(TAG_RDO206A, "salinity read successfully");
        return chemins_value.Rdo206AValue.Salinity;
    }
    else
    {
        ESP_LOGE(TAG_RDO206A, "Failed to read salinity value");
        return 0;
    }
}

CHEMINS_STATE rdo206a_write_temperature(uart_port_t uart_num , uint16_t num_registers)
{
    if(num_registers == 0){
        num_registers = REG_TEMPERATURE_VALUE_RDO206A;
    }
    if(chemins_uart_write_data(uart_num, DEVICE_ADDRESS_RDO206A, REG_TEMPERATURE_VALUE_PHG206A, num_registers) == eCHEMINS_OK){
        ESP_LOGI(TAG_RDO206A, "Temperature written successfully");
        return eCHEMINS_OK;
    }
    else{
        ESP_LOGE(TAG_RDO206A, "Failed to write temperature");
        return eCHEMINS_FAIL;
    }
}

CHEMINS_STATE rdo206a_turn_on(uart_port_t uart_num)
{
    if(chemins_uart_write_data(uart_num, DEVICE_ADDRESS_RDO206A, REG_SWITCH_MACHINE_RDO_206A, 0x0001))
    {
        ESP_LOGI(TAG_RDO206A, "Turn on successful");
        return eCHEMINS_OK;
    }
    else
    {
        ESP_LOGE(TAG_RDO206A, "Failed to turn on");
        return eCHEMINS_FAIL;
    }
}

CHEMINS_STATE rdo206a_turn_off(uart_port_t uart_num)
{
    if(chemins_uart_write_data(uart_num, DEVICE_ADDRESS_RDO206A, REG_SWITCH_MACHINE_RDO_206A, 0x0000))
    {
        ESP_LOGI(TAG_RDO206A, "Turn off successful");
        return eCHEMINS_OK;
    }
    else
    {
        ESP_LOGE(TAG_RDO206A, "Failed to turn off");
        return eCHEMINS_FAIL;
    }
}
