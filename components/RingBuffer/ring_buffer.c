#include "ring_buffer.h"
#include <stdio.h>

RingbufHandle_t g_ringbuf;
const char *C_RING_BUFFER = "C_RING_BUFFER";

void init_ring_buffer() {
    g_ringbuf = xRingbufferCreate(RINGBUFFER_SIZE, RINGBUF_TYPE_NOSPLIT);
    if (g_ringbuf == NULL)
    {
        printf("Failed to create ring buffer\n");
        // Xử lý lỗi nếu cần
    } else
    {
        printf("Ring buffer initialized successfully\n");
    }
}

void write_data_to_ringbuffer(SensorsDataBuffer *data)
{
    if (g_ringbuf == NULL)
    {
        printf("Ring buffer is not initialized\n");
        return;
    }

    BaseType_t ret = xRingbufferSend(g_ringbuf, data, sizeof(SensorsDataBuffer), 0);
    if (ret != pdTRUE)
    {
        printf("Failed to send data to ring buffer: Buffer is full\n");

        // Đọc một dữ liệu từ ring buffer để giải phóng chỗ
        size_t item_size;
        SensorsDataBuffer *read_data = (SensorsDataBuffer *)xRingbufferReceive(g_ringbuf, &item_size, pdMS_TO_TICKS(100));
        if (read_data != NULL)
        {
            printf("Read data from ring buffer: index=%d\n", read_data->index);
            vRingbufferReturnItem(g_ringbuf, (void *)read_data);
        }
        else 
        {
            printf("Failed to read data from ring buffer\n");
        }

        // Gửi lại dữ liệu mới vào buffer sau khi đã giải phóng chỗ
        ret = xRingbufferSend(g_ringbuf, data, sizeof(SensorsDataBuffer), 0);
        if (ret == pdTRUE)
        {
            printf("Data written to ring buffer: index=%d\n", data->index);
        }
        else
        {
            printf("Failed to send data to ring buffer after freeing space\n");
        }
    }
    else 
    {
        printf("Data written to ring buffer: index=%d\n", data->index);
    }
}

void read_all_data_from_ringbuffer() {
    size_t item_size;
    SensorsDataBuffer *read_data_buffer_all;

    // Đọc dữ liệu từ ring buffer cho đến khi ring buffer trống
    while ((read_data_buffer_all = (SensorsDataBuffer *)xRingbufferReceive(g_ringbuf, &item_size, pdMS_TO_TICKS(100))) != NULL) {
        printf("Read data from ring buffer: index=%d\n", read_data_buffer_all->index);
        vRingbufferReturnItem(g_ringbuf, (void *)read_data_buffer_all);
    }

    if (read_data_buffer_all == NULL) {
        printf("Failed to read data from ring buffer\n");
    }
}