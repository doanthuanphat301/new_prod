#ifndef RING_BUFFER_H
#define RING_BUFFER_H

#include "freertos/FreeRTOS.h"
#include "freertos/ringbuf.h"
#include "common_chemins.h"

#define RINGBUFFER_SIZE 2048

typedef struct {
    Phg206a PghValueBuffer;
    Rdo206a RdoValueBuffer;
    int index;
} SensorsDataBuffer;

extern RingbufHandle_t g_ringbuf;

void init_ring_buffer();
void write_data_to_ringbuffer(SensorsDataBuffer *data);
void read_all_data_from_ringbuffer();

#endif /* RING_BUFFER_H */