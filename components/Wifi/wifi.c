#include "wifi.h"

#define QRCODE_BASE_URL         "https://espressif.github.io/esp-jumpstart/qrcode.html"
#define MANUAL_WIFI_CONNECT true
#define AUTOMATIC_WIFI_CONNECT false

static const char *WIFI_TAG = "Wifi";
const char *TIMESTAMP_TAG = "Timestamp";

static WIFI_STATE g_state_wifi = eWIFI_DISCONNECTED;
EventGroupHandle_t g_wifi_event_group;
const int g_constant_ConnectedBit = BIT0;
const int g_connected_by_scan = BIT7;
const int g_connected_by_provision = BIT6;
const int g_connected = BIT6 | BIT7;
bool g_wifi_changed = false;
WifiConfig wifiConfigs[MAX_WIFI_CONFIGS];
bool wifi_prov_error = false;

void set_timestamp(time_t unix_timestamp)
{
    struct timeval tv = { .tv_sec = unix_timestamp };
    settimeofday(&tv, NULL);
}

static void time_sync_notification_cb(struct timeval *tv)
{
    ESP_LOGI(TIMESTAMP_TAG, "Notification of a time synchronization event");
}

void sync_timestamp(void)
{
    time_t now = 0;
    struct tm timeinfo = { 0 };
    time(&now);
    localtime_r(&now, &timeinfo);

    // Is time set? If not, tm_year will be (1970 - 1900).
    if (timeinfo.tm_year < (2016 - 1900)) {
        ESP_LOGW(TIMESTAMP_TAG, "Time is not set yet. Initializing and starting SNTP.");
        esp_sntp_config_t config = ESP_NETIF_SNTP_DEFAULT_CONFIG(CONFIG_SNTP_TIME_SERVER);
        config.sync_cb = time_sync_notification_cb;     // Note: This is only needed if we want
        esp_netif_sntp_init(&config);

        // wait for time to be set
        int retry = 0;
        const int retry_count = 10;
        while (esp_netif_sntp_sync_wait(2000 / portTICK_PERIOD_MS) == ESP_ERR_TIMEOUT && ++retry < retry_count) {
            ESP_LOGW(TIMESTAMP_TAG, "Waiting for system time to be set... (%d/%d)", retry, retry_count);
        }

        // update 'now' variable with current time
        time(&now);
        localtime_r(&now, &timeinfo);
        esp_netif_sntp_deinit();
    }

    char strftime_buf[64];
    // Set timezone to Vietnam Standard Time
    setenv("TZ", "ICT-7", 1);
    tzset();
    localtime_r(&now, &timeinfo);
    strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
    ESP_LOGW(TIMESTAMP_TAG, "The current date/time in Vietnam is: %s", strftime_buf);
}

void wifi_event_handler(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data)
{
    if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_START)
    {
        ESP_LOGW(WIFI_TAG, "Wi-Fi started");
        esp_wifi_connect();  // Attempt to connect to the AP
    }
    else if (event_base == WIFI_EVENT && event_id == WIFI_EVENT_STA_DISCONNECTED)
    {
        ESP_LOGE(WIFI_TAG, "Disconnected from AP");
        xEventGroupClearBits(g_wifi_event_group, g_constant_ConnectedBit);
        g_state_wifi=eWIFI_DISCONNECTED;
    }
    else if (event_base == IP_EVENT && event_id == IP_EVENT_STA_GOT_IP)
    {
        g_state_wifi=eWIFI_CONNECTED;
        ip_event_got_ip_t* event = (ip_event_got_ip_t*)event_data;
        ESP_LOGI(WIFI_TAG, "Got IP address: " IPSTR, IP2STR(&event->ip_info.ip));
        ESP_LOGI(WIFI_TAG, "Connected to AP!");
        if(UDP_ENABLE)
        {
            printf("%d\n",id_to_port(g_envisor_id));
            udp_logging_init(CONFIG_LOG_UDP_SERVER_IP, id_to_port(g_envisor_id), udp_logging_vprintf);
        }
        xEventGroupSetBits(g_wifi_event_group, g_constant_ConnectedBit);
    }
}
void prov_event_handler(void* arg, esp_event_base_t event_base, int32_t event_id, void* event_data)
{
    if (event_base == WIFI_PROV_EVENT)
    {
        switch (event_id) {
            case WIFI_PROV_START:
                ESP_LOGI(WIFI_TAG, "Provisioning started");
                break;
            case WIFI_PROV_CRED_RECV: {
                wifi_sta_config_t *wifi_sta_cfg = (wifi_sta_config_t *)event_data;
                ESP_LOGI(WIFI_TAG, "Received Wi-Fi credentials"
                        "\n\tSSID     : %s\n\tPassword : %s",
                        (const char *) wifi_sta_cfg->ssid,
                        (const char *) wifi_sta_cfg->password);
                add_newconfig((const char *) wifi_sta_cfg->ssid, (const char *) wifi_sta_cfg->password, false);
                break;
            }
            case WIFI_PROV_CRED_FAIL: {
                wifi_prov_sta_fail_reason_t *reason = (wifi_prov_sta_fail_reason_t *)event_data;
                ESP_LOGE(WIFI_TAG, "Provisioning failed!\n\tReason : %s"
                        "\n\tPlease reset to factory and retry provisioning",
                        (*reason == WIFI_PROV_STA_AUTH_ERROR) ?
                        "Wi-Fi station authentication failed" : "Wi-Fi access-point not found");
                        ESP_LOGE(WIFI_TAG, "Provisioning failed, rebooting...");
                        esp_wifi_restore();
                        vTaskDelay(5000 / portTICK_PERIOD_MS);
                        esp_restart();
                break;
            }
            case WIFI_PROV_CRED_SUCCESS:
                ESP_LOGI(WIFI_TAG, "Provisioning successful");
                break;
            case WIFI_PROV_END:
                /* De-initialize manager once provisioning is finished */
                ESP_LOGI(WIFI_TAG, "Provisioning ended");
                xEventGroupSetBits(g_wifi_event_group, g_connected_by_provision);
                break;
            default:
                break;
        }
    }
    else if (event_base == PROTOCOMM_TRANSPORT_BLE_EVENT)
    {
        switch (event_id) {
            case PROTOCOMM_TRANSPORT_BLE_CONNECTED:
                ESP_LOGI(WIFI_TAG, "BLE transport: Connected!");
                break;
            case PROTOCOMM_TRANSPORT_BLE_DISCONNECTED:
                ESP_LOGI(WIFI_TAG, "BLE transport: Disconnected!");
                break;
            default:
                break;
        }
    }
    else if (event_base == PROTOCOMM_SECURITY_SESSION_EVENT)
    {
        switch (event_id) {
            case PROTOCOMM_SECURITY_SESSION_SETUP_OK:
                ESP_LOGI(WIFI_TAG, "Secured session established!");
                break;
            case PROTOCOMM_SECURITY_SESSION_INVALID_SECURITY_PARAMS:
                ESP_LOGE(WIFI_TAG, "Received invalid security parameters for establishing secure session!");
                break;
            case PROTOCOMM_SECURITY_SESSION_CREDENTIALS_MISMATCH:
                ESP_LOGE(WIFI_TAG, "Received incorrect username and/or PoP for establishing secure session!");
                break;
            default:
                break;
        }
    }
}
void wifi_stack_init()
{
    wifi_init_config_t cfg = WIFI_INIT_CONFIG_DEFAULT();
    ESP_ERROR_CHECK(esp_wifi_init(&cfg));  // Initialize the Wi-Fi stack

    esp_event_handler_instance_t instance_any_id;
    esp_event_handler_instance_t instance_got_ip;

    g_wifi_event_group = xEventGroupCreate();

    ESP_ERROR_CHECK(esp_event_handler_register(WIFI_PROV_EVENT, ESP_EVENT_ANY_ID, &prov_event_handler, NULL));
    ESP_ERROR_CHECK(esp_event_handler_register(PROTOCOMM_TRANSPORT_BLE_EVENT,
                                                        ESP_EVENT_ANY_ID,
                                                        &prov_event_handler,
                                                        NULL));
    ESP_ERROR_CHECK(esp_event_handler_register(PROTOCOMM_SECURITY_SESSION_EVENT,
                                                        ESP_EVENT_ANY_ID,
                                                        &prov_event_handler,
                                                        NULL));
    ESP_ERROR_CHECK(esp_event_handler_instance_register(WIFI_EVENT,
                                                        ESP_EVENT_ANY_ID,
                                                        &wifi_event_handler,
                                                        NULL,
                                                        &instance_any_id));  // Register event handler for Wi-Fi events
    ESP_ERROR_CHECK(esp_event_handler_instance_register(IP_EVENT,
                                                        IP_EVENT_STA_GOT_IP,
                                                        &wifi_event_handler,
                                                        NULL,
                                                        &instance_got_ip));  // Register event handler for IP events
    ESP_ERROR_CHECK(esp_wifi_set_mode(WIFI_MODE_STA));
}
void wifi_prov_print_qr(const char *name, const char *username, const char *pop, const char *transport)
{
    if (!name || !transport) {
        ESP_LOGW(WIFI_TAG, "Cannot generate QR code payload. Data missing.");
        return;
    }
    char payload[150] = {0};
    if (pop)
    {
        snprintf(payload, sizeof(payload), "{\"ver\":\"%s\",\"name\":\"%s\"" \
                    ",\"pop\":\"%s\",\"transport\":\"%s\"}",
                    "v1", name, pop, transport);
    }
    ESP_LOGI(WIFI_TAG, "Scan this QR code from the provisioning application for Provisioning.");
    esp_qrcode_config_t cfg = ESP_QRCODE_CONFIG_DEFAULT();
    esp_qrcode_generate(&cfg, payload);
    ESP_LOGI(WIFI_TAG, "If QR code is not visible, copy paste the below URL in a browser.\n%s?data=%s", QRCODE_BASE_URL, payload);
    vTaskDelay(100 / portTICK_PERIOD_MS);
}
void get_device_service_name(char *service_name, size_t max)
{
    uint8_t eth_mac[6];
    const char *ssid_prefix = "envisor_id_";
    esp_wifi_get_mac(WIFI_IF_STA, eth_mac);
    snprintf(service_name, max, "%s%02X%02X%02X", ssid_prefix, eth_mac[3], eth_mac[4], eth_mac[5]);
}
WIFI_CONFIG_STATE read_wifi_config(void)
{
    nvs_handle_t nvs_handle;
    esp_err_t err;

    // Initialize NVS
    err = nvs_open_from_partition("nvs_data","wifi_store", NVS_READWRITE, &nvs_handle);
    if (err != ESP_OK)
    {
        ESP_LOGE(WIFI_TAG, "Error opening NVS handle!");
        return eWIFI_CONFIG_ERROR;
    }
    // Load existing configurations
    size_t required_size = MAX_WIFI_CONFIGS * sizeof(WifiConfig);
    err = nvs_get_blob(nvs_handle, "wifi_configs", wifiConfigs, &required_size);
    if (err == ESP_ERR_NVS_NOT_FOUND) {
        // First time initialization
        ESP_LOGI(WIFI_TAG, "No configurations found.....Using default wifi configratuion");
        memset(wifiConfigs, 0, required_size);
        return eWIFI_CONFIG_NOT_INITIALIZED;
    } else if (err != ESP_OK) {
        ESP_LOGE(WIFI_TAG, "Error reading NVS!");
        nvs_close(nvs_handle);
        return eWIFI_CONFIG_ERROR;
    }
    else
    {
        ESP_LOGI(WIFI_TAG, "Configurations loaded successfully");
        return eWIFI_CONFIG_INITIALIZED;
    }
}
int list_wifi_stored(void)
{
    int config_count = 0;
    read_wifi_config();
    for(int i=0;i<MAX_WIFI_CONFIGS;i++)
    {
        ESP_LOGI(WIFI_TAG,"SSID: %s, password: %s, length: %d",wifiConfigs[i].ssid, wifiConfigs[i].password, strlen(wifiConfigs[i].ssid));
        if (strlen(wifiConfigs[i].ssid) > 0)
        {
            config_count++;
        }
    }
    return config_count;
}
WIFI_STATE connect_wifi(char *ssid, char *password, bool change)
{
    wifi_config_t wifi_config;
    if(change)
    {
        ESP_LOGI(WIFI_TAG, "Disconnecting from current WiFi...");
        ESP_ERROR_CHECK(esp_wifi_disconnect());
    }
    wifi_config = (wifi_config_t) {
        .sta = {
            .threshold.authmode = -127,
            .sae_pwe_h2e = WPA3_SAE_PWE_BOTH,
            .sae_h2e_identifier = "",
        },
    };
    strcpy((char *)wifi_config.sta.ssid, ssid);
    strcpy((char *)wifi_config.sta.password, password);
    ESP_ERROR_CHECK(esp_wifi_set_config(WIFI_IF_STA, &wifi_config));  // Set Wi-Fi configuration
    ESP_LOGI(WIFI_TAG,"Ready to connect to ssid: %s    password: %s",(char *)wifi_config.sta.ssid,(char *)wifi_config.sta.password);
    esp_wifi_connect();
    ESP_LOGW(WIFI_TAG,"Connecting to new wifi..............");
    g_wifi_changed = false;
    vTaskDelay(15000 / portTICK_PERIOD_MS);
    return checkwifi_state();
}
void add_newconfig(const char* ssid, const char* password, bool change)
{
    nvs_handle_t nvs_handle;
    esp_err_t err;
    // Initialize NVS
    err = nvs_open_from_partition("nvs_data","wifi_store", NVS_READWRITE, &nvs_handle);
    if (err != ESP_OK)
    {
        ESP_LOGE(WIFI_TAG, "Error opening NVS handle!");
        return;
    }

    // Load existing configurations
    size_t required_size = MAX_WIFI_CONFIGS * sizeof(WifiConfig);
    err = nvs_get_blob(nvs_handle, "wifi_configs", wifiConfigs, &required_size);
    if (err == ESP_ERR_NVS_NOT_FOUND) {
        // First time initialization
        ESP_LOGI(WIFI_TAG, "No configurations found, adding new one.");
        memset(wifiConfigs, 0, required_size);
    } else if (err != ESP_OK) {
        ESP_LOGE(WIFI_TAG, "Error reading NVS!");
        nvs_close(nvs_handle);
        return;
    }
    //check if the ssid already exists
    for(int i = 0; i < MAX_WIFI_CONFIGS; i++)
    {
        if (strcmp(wifiConfigs[i].ssid, ssid) == 0)
        {
            {
                ESP_LOGI(WIFI_TAG, "SSID already exists, index: %d", i);
                //Delete all duplicated SSID and password entries if they exist, ensure all configs are unique
                memset(wifiConfigs[i].ssid,0,strlen(wifiConfigs[i].ssid));
                memset(wifiConfigs[i].password,0,strlen(wifiConfigs[i].password));
            }
        }
    }
    // Move all configurations, not leaving any gaps between configs
    for(int i = 1;i < MAX_WIFI_CONFIGS - 1; i++)
    {
        if(strlen(wifiConfigs[i].ssid) > 0 && strlen(wifiConfigs[i-1].ssid) == 0)
        {
            int j = i;
            while(strlen(wifiConfigs[j-1].ssid) == 0)
            {
                wifiConfigs[j-1] = wifiConfigs[j];
                memset(wifiConfigs[j].ssid,0,strlen(wifiConfigs[j].ssid));
                memset(wifiConfigs[j].password,0,strlen(wifiConfigs[j].password));
                j--;
            }
        }
    }
    // Add the new config to the beginning and shift others down
    for (int i = MAX_WIFI_CONFIGS - 1; i > 0; i--) {
        wifiConfigs[i] = wifiConfigs[i - 1];
    }
    strncpy(wifiConfigs[0].ssid, ssid, sizeof(wifiConfigs[0].ssid) - 1);
    wifiConfigs[0].ssid[sizeof(wifiConfigs[0].ssid) - 1] = '\0';
    strncpy(wifiConfigs[0].password, password, sizeof(wifiConfigs[0].password) - 1);
    wifiConfigs[0].password[sizeof(wifiConfigs[0].password) - 1] = '\0';
    // Save the updated configurations
    err = nvs_set_blob(nvs_handle, "wifi_configs", wifiConfigs, required_size);
    if (err != ESP_OK) {
        ESP_LOGE(WIFI_TAG, "Error saving to NVS!");
        nvs_close(nvs_handle);
        return;
    }
    err = nvs_commit(nvs_handle);
    if (err != ESP_OK) {
        ESP_LOGE(WIFI_TAG, "Error committing to NVS!");
    }
    nvs_close(nvs_handle);
    if(change)
    {
        g_wifi_changed = true;
    }
}
WIFI_STATE checkwifi_state(void)
{
    return g_state_wifi;
}
void set_wifi_max_tx_power(void)
{
    esp_err_t err = esp_wifi_set_max_tx_power(20);
    // Kiểm tra và log kết quả
    ESP_LOGE(WIFI_TAG, "Esp set power: %s", esp_err_to_name(err));

    int8_t tx_power;
    esp_wifi_get_max_tx_power(&tx_power);
    tx_power=tx_power/4;
    ESP_LOGI(WIFI_TAG, "Current max TX power: %d dBm", tx_power);
}
void wifi_init()
{
    wifi_prov_mgr_config_t config = {
        .scheme = wifi_prov_scheme_ble,
        .scheme_event_handler = WIFI_PROV_SCHEME_BLE_EVENT_HANDLER_FREE_BTDM,
    };
    ESP_ERROR_CHECK(wifi_prov_mgr_init(config));
    char service_name[30];
    get_device_service_name(service_name, sizeof(service_name));
    wifi_prov_security_t security = WIFI_PROV_SECURITY_1;
    const char *pop = "unicorn2025";
    wifi_prov_security1_params_t *sec_params = pop;
    const char *service_key = NULL;
    uint8_t custom_service_uuid[16] = {
        /* LSB <---------------------------------------
            * ---------------------------------------> MSB */
        0xb4, 0xdf, 0x5a, 0x1c, 0x3f, 0x6b, 0xf4, 0xbf,
        0xea, 0x4a, 0x82, 0x03, 0x04, 0x90, 0x1a, 0x02,
    };
    wifi_prov_scheme_ble_set_service_uuid(custom_service_uuid);
    ESP_ERROR_CHECK(wifi_prov_mgr_start_provisioning(security, (const void *) sec_params, service_name, service_key));
    wifi_prov_print_qr(service_name, NULL, pop, "ble");
    scan_and_connect_wifi(MANUAL_WIFI_CONNECT);
    xEventGroupWaitBits(g_wifi_event_group, g_connected, pdTRUE, pdFALSE, portMAX_DELAY);
    ESP_LOGW(WIFI_TAG, "Wifi initialized");
    wifi_prov_mgr_deinit();
}
void wifi_task(void *arg)
{
    EventGroupHandle_t event_manager = (EventGroupHandle_t) arg;
    wifi_init();
    if(power_save() == ESP_OK)
    {
        ESP_LOGI(WIFI_TAG, "Power save mode enabled");
    }
    else
    {
        ESP_LOGE(WIFI_TAG, "Power save mode failed");
    }
    sync_timestamp();
    while(1)
    {
        if(g_wifi_changed)
        {
            ESP_LOGW(WIFI_TAG,"Changing wifi...");
            if(connect_wifi(wifiConfigs[0].ssid, wifiConfigs[0].password, true) == eWIFI_CONNECTED)
            {
                ESP_LOGI(WIFI_TAG,"Changed to wifi: %s",wifiConfigs[0].ssid);
            }
            else
            {
                ESP_LOGI(WIFI_TAG,"Failed to change wifi");
            }
            continue;
        }
        ESP_LOGI(WIFI_TAG,"Checking for wifi status...");
        if(checkwifi_state()==eWIFI_CONNECTED)
        {
            ESP_LOGI(WIFI_TAG,"Wifi is still connected");
            xEventGroupSetBits(event_manager, WIFI_CONNECTED_EVENT);
            vTaskDelay(30000 / portTICK_PERIOD_MS);
        }
        else if(checkwifi_state()==eWIFI_DISCONNECTED)
        {
            ESP_LOGI(WIFI_TAG,"Wifi is disconnected");
            xEventGroupClearBits(event_manager, WIFI_CONNECTED_EVENT);
            ESP_LOGI(WIFI_TAG, "Retry connect to Wifi");
            scan_and_connect_wifi(AUTOMATIC_WIFI_CONNECT);
        }
    }
}
void scan_and_connect_wifi(bool connect_type)
{
    int config_count = list_wifi_stored();
    if(config_count == 0)
    {
        ESP_LOGI(WIFI_TAG, "No wifi stored");
        return;
    }
    if(connect_type == AUTOMATIC_WIFI_CONNECT)
    {
        wifi_ap_record_t *ap_list;
        uint16_t ap_num;
        esp_wifi_scan_start(NULL, true);
        esp_wifi_scan_get_ap_num(&ap_num);
        ap_list = (wifi_ap_record_t *)malloc(sizeof(wifi_ap_record_t) * ap_num);
        esp_wifi_scan_get_ap_records(&ap_num, ap_list);
        for(int i = 0 ;i < ap_num; i++)
        {
            ESP_LOGI(WIFI_TAG,"SSID: %s, RSSI: %d",ap_list[i].ssid, ap_list[i].rssi);
        }
        for(int i = 0 ; i< MAX_WIFI_CONFIGS; ++i)
        {
            for(int j = 0 ;j < ap_num; j++)
            {
                if(strcmp(wifiConfigs[i].ssid, (char *)ap_list[j].ssid) == 0)
                {
                    ESP_LOGI(WIFI_TAG,"Found the ssid: %s",wifiConfigs[i].ssid);
                    if(connect_wifi(wifiConfigs[i].ssid, wifiConfigs[i].password, false) == eWIFI_CONNECTED)
                    {
                        ESP_LOGI(WIFI_TAG,"Connected to wifi: %s",wifiConfigs[i].ssid);
                        return;
                    }
                    else continue;
                }
            }
        }
        ESP_LOGI(WIFI_TAG,"No wifi available");
    }
    else if(connect_type == MANUAL_WIFI_CONNECT)
    {
        for(int i = 0 ;i < config_count; i++)
        {
            if(connect_wifi(wifiConfigs[i].ssid, wifiConfigs[i].password, false) == eWIFI_CONNECTED)
            {
                ESP_LOGI(WIFI_TAG,"Connected to wifi: %s",wifiConfigs[i].ssid);
                xEventGroupSetBits(g_wifi_event_group, g_connected_by_scan);
                return;
            }
            else continue;
        }
    }
}
