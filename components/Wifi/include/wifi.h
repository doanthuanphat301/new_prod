#ifndef WIFI_H
#define WIFI_H
#include "global.h"
#include "wifi_provisioning/manager.h"
#include "wifi_provisioning/scheme_ble.h"
#include "qrcode.h"
#include "udp_logging.h"

#define MAX_WIFI_CONFIGS 5


typedef struct {
    char ssid[32];
    char password[64];
} WifiConfig;

typedef enum
{
    eWIFI_CONFIG_NOT_INITIALIZED = 0,
    eWIFI_CONFIG_INITIALIZED = 1,
    eWIFI_CONFIG_ERROR = 2,
}WIFI_CONFIG_STATE;
typedef enum {
    eWIFI_CONNECTED     = 0,
    eWIFI_DISCONNECTED  = 1
}WIFI_STATE;

void wifi_event_handler(void* arg, esp_event_base_t event_base,int32_t event_id, void* event_data);
void wifi_stack_init(void);
void wifi_prov_print_qr(const char *name, const char *username, const char *pop, const char *transport);
void get_device_service_name(char *service_name, size_t max);
WIFI_CONFIG_STATE read_wifi_config(void);
int list_wifi_stored(void);
WIFI_STATE connect_wifi(char *ssid, char *password, bool change);
void add_newconfig(const char* ssid, const char* password, bool change);
WIFI_STATE checkwifi_state(void);
void set_wifi_max_tx_power(void);
void wifi_init();
void wifi_task(void *arg);
void scan_and_connect_wifi(bool connect_type);

void sync_timestamp(void);
void set_timestamp(time_t unix_timestamp);
#endif // WIFI_H