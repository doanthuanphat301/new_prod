# PHG-206A Device Library

This library provides an interface for reading and writing data to the PHG-206A device via UART. The library is built using FreeRTOS and includes functions for reading pH and temperature values, performing calibrations, and managing device settings.

## Features

- Read and write data via UART- RS485
- Configure UART settings
- Get pH and temperature values
- Perform zero and slope calibrations
- Reset device to factory settings
- Manage device address

## API in library

- phg206a_uart_read_data
- phg206a_uart_write_data
- pgh206a_config
- pgh206a_get_data
- pgh206a_reset_factory
- pgh206a_write_6_86_zero_calibration
- pgh206a_write_4_zero_calibration
- pgh206a_write_9_18_zero_calibration
- pgh206a_read_zero_calibration
- pgh206a_slope_calibration
- pgh206a_write_temperature
- pgh206a_read_temperature
- pgh206a_read_device_address
- pgh206a_write_device_address

## future of api in library
- pgh206a_config :
  default : uart : 9600, 8 data bit, 1 stop bit, UART_MODE_RS485_HALF_DUPLEX


- phg206a_uart_read_data : read data that pgh206a send, timeout read data: default 1s, set : TIMEOUT_WAIT_RESPONSE_DATA_READ in include/pgh206a.h

- phg206a_uart_write_data : write data in pgh206a , timeout read response data: default 1s, set : TIMEOUT_WAIT_RESPONSE_DATA_READ in include/pgh206a.h

- pgh206a_get_data : get data after mcu request data from pgh206s
  struct:
    float   pH;               /**< pH value */
    float   temp;             /**< Temperature value */
    float zero_calib;         /**< Zero calibration value */
    float slope_calib;        /**< Slope calibration value */
    uint8_t device_address;   /**< Device address */

- pgh206a_reset_factory: reset factory pgh206a

## calibration will be use this api
- pgh206a_write_6_86_zero_calibration: 
- pgh206a_write_4_zero_calibration
- pgh206a_write_9_18_zero_calibration
- pgh206a_read_zero_calibration
- pgh206a_slope_calibration
- pgh206a_write_temperature
- pgh206a_read_temperature

- pgh206a_calibration_auto: auto calibration for pgh206a
    step 1: send reset factory frame 
    step 2: send read data each 20s during the time is set
    step 3: send 6.86_calib frame when value stablity
    step 4: send read data each 20s during the time is set 
    step 5: send 4_calib frame when value stablity
    step 6: send read data each 20s during the time is set
    step 7: send 9_18_calib frame when value stablity

## future rs485 will be use this api
- pgh206a_read_device_address
- pgh206a_write_device_address

## All API is encoutered check_response_frame step, in pgh206a.c