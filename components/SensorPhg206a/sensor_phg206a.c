#include "sensor_phg206a.h"

extern CheminsValue chemins_value;
static const char *TAG_PHG206A = "PHG206A";
void phg206a_get_data(phg206a_value_t select_value_get,Phg206a *result)
{
    switch (select_value_get) {
        case PHG206A_PH_e:
            result->PH = chemins_value.Phg206AValue.PH;
            break;
        case PHG206A_TEMP_e:
            result->Temp = chemins_value.Phg206AValue.Temp;
            break;
        case PHG206A_ZERO_CALIB_e:
            result->ZeroCalib = chemins_value.Phg206AValue.ZeroCalib;
            break;
        case PHG206A_SLOPE_CALIB_e:
            result->SlopeCalib = chemins_value.Phg206AValue.SlopeCalib;
            break;
        case PHG206A_DEVICE_ADDRESS_e:
            result->DeviceAddress = chemins_value.Phg206AValue.DeviceAddress;
            break;
        default:
            ESP_LOGE(TAG_PHG206A, "Invalid select_value_get");
            break;
    }
}  

CHEMINS_STATE phg206a_read_measurement(uart_port_t uart_num)
{
    if(chemins_uart_read_data(uart_num, DEVICE_ADDRESS_PHG206A, REG_MEASURED_TEMPERATURE, REG_MEASURED_TEMPERATURE_NUM)){
        ESP_LOGI(TAG_PHG206A, "Measurement read successfully");
        return eCHEMINS_OK;
    }
    else{
        ESP_LOGE(TAG_PHG206A, "Failed to read measurement");
        return eCHEMINS_FAIL;
    }
}

CHEMINS_STATE phg206a_reset_factory(uart_port_t uart_num)
{
    if(chemins_uart_write_data(uart_num, DEVICE_ADDRESS_PHG206A, REG_FACTORY_RESET, REG_FACTORY_RESET_NUM))
    {
        ESP_LOGI(TAG_PHG206A, "Factory reset successful");
        return eCHEMINS_OK;
    }
    else{
        ESP_LOGE(TAG_PHG206A, "Factory reset failed");
        return eCHEMINS_FAIL;
    }
}

CHEMINS_STATE phg206a_write_6_86_zero_calibration(uart_port_t uart_num)
{
    if(chemins_uart_read_data(uart_num, DEVICE_ADDRESS_PHG206A, REG_ZERO_CALI_PH6_86_PHG206A, REG_ZERO_CALI_PH6_86_NUM_PHG206A))
    {
        ESP_LOGI(TAG_PHG206A, "6.86 zero calibration written successfully");
        return eCHEMINS_OK;
    }
    else
    {
        ESP_LOGE(TAG_PHG206A, "Failed to write 6.86 zero calibration");
        return eCHEMINS_FAIL;
    }
}


CHEMINS_STATE phg206a_write_4_zero_calibration(uart_port_t uart_num)
{ 
    if(chemins_uart_read_data(uart_num, DEVICE_ADDRESS_PHG206A, REG_SLOPE_CALI_PH4_PHG206A, REG_SLOPE_CALI_PH4_NUM_PHG206A) == eCHEMINS_OK)
    {
        ESP_LOGI(TAG_PHG206A, "4.0 zero calibration written successfully");
        return eCHEMINS_OK;
    }
    else{
        ESP_LOGE(TAG_PHG206A, "Failed to write 4.0 zero calibration");
        return eCHEMINS_FAIL;
    }
}

CHEMINS_STATE phg206a_write_9_18_zero_calibration(uart_port_t uart_num)
{ 
    if(chemins_uart_read_data(uart_num, DEVICE_ADDRESS_PHG206A, REG_SLOPE_CALI_PH9_18_PHG206A, REG_SLOPE_CALI_PH9_18_NUM_PHG206A)){
        ESP_LOGI(TAG_PHG206A, "9.18 zero calibration written successfully");
        return eCHEMINS_OK;
    }
    else{
        ESP_LOGE(TAG_PHG206A, "Failed to write 9.18 zero calibration");
        return eCHEMINS_FAIL;
    }
}

float phg206a_read_zero_calibration(uart_port_t uart_num)
{
    if(chemins_uart_read_data(uart_num, DEVICE_ADDRESS_PHG206A, REG_ZERO_CALI_VALUE_PHG206A, REG_ZERO_CALI_VALUE_NUM_PHG206A)){
        ESP_LOGI(TAG_PHG206A, "Zero calibration read successfully");
        return chemins_value.Phg206AValue.ZeroCalib;
    }
    else{
        ESP_LOGE(TAG_PHG206A, "Failed to read zero calibration");
        return 0;
    }
}

float phg206a_slope_calibration(uart_port_t uart_num)
{
    if(chemins_uart_write_data(uart_num, DEVICE_ADDRESS_PHG206A, REG_SLOPE_CALI_VALUE_PHG206A, REG_SLOPE_CALI_VALUE_NUM_PHG206A)){
        ESP_LOGI(TAG_PHG206A, "Slope calibration read successfully");
        return chemins_value.Phg206AValue.SlopeCalib;
    }
    else{
        ESP_LOGE(TAG_PHG206A, "Failed to read slope calibration");
        return 0;
    }
}

CHEMINS_STATE phg206a_write_temperature(uart_port_t uart_num , uint16_t num_registers)
{
    if(num_registers == 0){
        num_registers = REG_TEMPERATURE_VALUE_NUM_PHG206A;
    }
    if(chemins_uart_write_data(uart_num, DEVICE_ADDRESS_PHG206A, REG_TEMPERATURE_VALUE_PHG206A, num_registers) == eCHEMINS_OK){
        ESP_LOGI(TAG_PHG206A, "Temperature written successfully");
        return eCHEMINS_OK;
    }
    else{
        ESP_LOGE(TAG_PHG206A, "Failed to write temperature");
        return eCHEMINS_FAIL;
    }
}

float phg206a_read_temperature(uart_port_t uart_num)
{
    if(chemins_uart_read_data(uart_num, DEVICE_ADDRESS_PHG206A, REG_TEMPERATURE_VALUE_PHG206A, REG_TEMPERATURE_VALUE_NUM_PHG206A))
    {
        ESP_LOGI(TAG_PHG206A, "Temperature read successfully");
        return chemins_value.Phg206AValue.Temp;
    }
    else
    {
        ESP_LOGE(TAG_PHG206A, "Failed to read temperature");
        return 0;
    }
}

uint8_t phg206a_read_device_address(uart_port_t uart_num)
{
    if(chemins_uart_read_data(uart_num, DEVICE_ADDRESS_PHG206A, REG_DEVICE_ADDRESS, REG_DEVICE_ADDRESS_NUM))
    {
        ESP_LOGI(TAG_PHG206A, "Device address read successfully");
        return chemins_value.Phg206AValue.DeviceAddress;
    }
    else
    {
        ESP_LOGE(TAG_PHG206A, "Failed to read device address");
        return 0;
    }
}

CHEMINS_STATE phg206a_write_device_address(uart_port_t uart_num, uint16_t num_registers)
{
    if(chemins_uart_write_data(uart_num, DEVICE_ADDRESS_PHG206A, REG_DEVICE_ADDRESS, num_registers) == eCHEMINS_OK)
    {
        ESP_LOGI(TAG_PHG206A, "Device address [%d] written successfully", num_registers);
        return eCHEMINS_OK;
    }
    else{
        ESP_LOGE(TAG_PHG206A, "Failed to write device address");
        return eCHEMINS_FAIL;
    }
}

// void PHG206a_calibration_auto(uart_port_t uart_num, uint8_t min)
// {
//     ESP_LOGW(TAG_PHG206A,"Starting calibration");
//     if(PHG206a_reset_factory(uart_num)){
//         for(uint8_t i = 0; i < min*3; i++){
//             chemins_uart_read_data(uart_num, DEVICE_ADDRESS_PHG206A, REG_MEASURED_TEMPERATUE, REG_MEASURED_TEMPERATUE_NUM);
//             ESP_LOGW(TAG_PHG206A,"pH value: [%0.2f]", chemins_value.Phg206AValue.PH);
//             ESP_LOGW(TAG_PHG206A,"Temperature value: [%0.2f]", chemins_value.Phg206AValue.Temp);
//             vTaskDelay(20000/portTICK_PERIOD_MS);
//         }

//         PHG206a_write_6_86_zero_calibration(uart_num);
//         for(uint8_t i = 0; i < min*3; i++){
//             chemins_uart_read_data(uart_num, DEVICE_ADDRESS_PHG206A, REG_MEASURED_TEMPERATUE, REG_MEASURED_TEMPERATUE_NUM);
//             ESP_LOGW(TAG_PHG206A,"pH value: [%0.2f]", chemins_value.Phg206AValue.PH);
//             ESP_LOGW(TAG_PHG206A,"Temperature value: [%0.2f]", chemins_value.Phg206AValue.Temp);
//             vTaskDelay(20000/portTICK_PERIOD_MS);
//         }

//         PHG206a_write_4_zero_calibration(uart_num);
//         for(uint8_t i = 0; i < min*3; i++){
//             chemins_uart_read_data(uart_num, DEVICE_ADDRESS_PHG206A, REG_MEASURED_TEMPERATUE, REG_MEASURED_TEMPERATUE_NUM);
//             ESP_LOGW(TAG_PHG206A,"pH value: [%0.2f]", chemins_value.Phg206AValue.PH);
//             ESP_LOGW(TAG_PHG206A,"Temperature value: [%0.2f]", chemins_value.Phg206AValue.Temp);
//             vTaskDelay(20000/portTICK_PERIOD_MS);
//         }

//         PHG206a_write_9_18_zero_calibration(uart_num);
//         ESP_LOGW(TAG_PHG206A,"Calibration complete");
//     }
//     else{
//         ESP_LOGW(TAG_PHG206A,"Calibration failed");
//     }
// }
