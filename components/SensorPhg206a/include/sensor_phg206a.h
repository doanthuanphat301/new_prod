#pragma once

#include "global.h"
#include "crc16.h"
#include "common_chemins.h"
#include "register_common_chemins.h"
/**
 * @brief Structure to hold pH and temperature values along with calibration and device address.
 */
// struct phg206_a_value {
//   float   pH;               /**< pH value */
//   float   temp;             /**< Temperature value */
//   float zero_calib;         /**< Zero calibration value */
//   float slope_calib;        /**< Slope calibration value */
//   uint8_t device_address;   /**< Device address */
// };

/**
 * @brief Enumeration to select the type of value to get from the device.
 */
typedef enum  {
    PHG206A_PH_e = 0,
    PHG206A_TEMP_e = 1,
    PHG206A_ZERO_CALIB_e = 2,
    PHG206A_SLOPE_CALIB_e = 3,
    PHG206A_DEVICE_ADDRESS_e = 4
} phg206a_value_t;

CHEMINS_STATE phg206a_read_measurement(uart_port_t uart_num);
/**
 * @brief Gets data from the device.
 * 
 * @param select_value_get Type of value to get
 * @param result Pointer to the structure to hold the result
 */
void phg206a_get_data(phg206a_value_t select_value_get,Phg206a *result);

/**
 * @brief Resets the device to factory settings.
 * 
 * @param uart_num UART port number
 * @return true if reset is successful, false otherwise
 */
CHEMINS_STATE phg206a_reset_factory(uart_port_t uart_num);

/**
 * @brief Writes zero calibration value for pH 6.86.
 * 
 * @param uart_num UART port number
 */
CHEMINS_STATE phg206a_write_6_86_zero_calibration(uart_port_t uart_num);

/**
 * @brief Writes zero calibration value for pH 4.0.
 * 
 * @param uart_num UART port number
 */
CHEMINS_STATE phg206a_write_4_zero_calibration(uart_port_t uart_num);

/**
 * @brief Writes zero calibration value for pH 9.18.
 * 
 * @param uart_num UART port number
 */
CHEMINS_STATE phg206a_write_9_18_zero_calibration(uart_port_t uart_num);

/**
 * @brief Reads zero calibration value.
 * 
 * @param uart_num UART port number
 * @return Zero calibration value
 */
float phg206a_read_zero_calibration(uart_port_t uart_num);

/**
 * @brief Reads slope calibration value.
 * 
 * @param uart_num UART port number
 * @return Slope calibration value
 */
float phg206a_slope_calibration(uart_port_t uart_num);

/**
 * @brief Writes temperature value.
 * 
 * @param uart_num UART port number
 * @param num_registers Number of registers to write : if 25.8 *C -> x10 : 258 (DEC)
 */
CHEMINS_STATE phg206a_write_temperature(uart_port_t uart_num , uint16_t num_registers);

/**
 * @brief Reads temperature value.
 * 
 * @param uart_num UART port number
 * @return Temperature value
 */
float phg206a_read_temperature(uart_port_t uart_num);

/**
 * @brief Reads device address.
 * 
 * @param uart_num UART port number
 * @return Device address
 */
uint8_t phg206a_read_device_address(uart_port_t uart_num);

/**
 * @brief Writes device address.
 * 
 * @param uart_num UART port number
 * @param num_registers Number of registers to write
 */
CHEMINS_STATE phg206a_write_device_address(uart_port_t uart_num, uint16_t num_registers);