/* UART Events Example

   This example code is in the Public Domain (or CC0 licensed, at your option.)

   Unless required by applicable law or agreed to in writing, this
   software is distributed on an "AS IS" BASIS, WITHOUT WARRANTIES OR
   CONDITIONS OF ANY KIND, either express or implied.
*/
#include <stdio.h>
#include <string.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/queue.h"
#include "driver/uart.h"
#include "esp_log.h"
#include "driver/gpio.h"
#include "crc16.h"
#include "phg206a.h"
static const char *TAG = "uart_pgh206a";

#define EX_UART_NUM UART_NUM_1
#define BUF_SIZE (1024)
struct  pgh206_a_value pgh206a_data;

static void uart_read_pH_task(void *pvParameters){
    for(;;){
        //read pH and temp
        phg206a_uart_read_data(EX_UART_NUM, REG_MEASURED_TEMPERATUE, REG_MEASURED_TEMPERATUE_NUM);
        // get ph and temp 
        pgh206a_get_data(pH_e, &pgh206a_data);
        pgh206a_get_data(temp_e, &pgh206a_data);
        ESP_LOGI(TAG,"value temp[%0.2f]", pgh206a_data.temp);
        ESP_LOGI(TAG,"value PHHHh[%0.2f]", pgh206a_data.pH);
        //read device address
        vTaskDelay(5000/portTICK_PERIOD_MS);
        pgh206a_read_device_address(EX_UART_NUM);
        //get device address
        pgh206a_get_data(device_address_e, &pgh206a_data);
        ESP_LOGI(TAG,"value PHHHh[%u]", pgh206a_data.device_address);
        vTaskDelay(20000/portTICK_PERIOD_MS);
    }
}

void app_main(void)
{
    esp_log_level_set(TAG, ESP_LOG_INFO);
    pgh206a_config(EX_UART_NUM, GPIO_NUM_17, GPIO_NUM_18, BUF_SIZE *2 , BUF_SIZE * 2, 2);

    //Set UART log level
    esp_log_level_set(TAG, ESP_LOG_INFO);
    vTaskDelay(1000/portTICK_PERIOD_MS);
    xTaskCreate(uart_read_pH_task, "uart_event_task", 2048, NULL, 12, NULL);
}