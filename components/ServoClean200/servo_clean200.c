#include "servo_clean200.h"
const char *TAG_CLEAN200 = "CLEAN200";

extern CheminsValue chemins_value;
CHEMINS_STATE clean_reset_factory(uart_port_t uart_num)
{
    if(chemins_uart_read_data(uart_num, DEVICE_ADDRESS_CLEAN200, 0x2020, 0x0000))
    {
        ESP_LOGI(TAG_CLEAN200, "reset factory successful");
        return eCHEMINS_OK;
    }
    else
    {
        ESP_LOGE(TAG_CLEAN200, "reset factory failure");
        return eCHEMINS_FAIL;
    }
}

CHEMINS_STATE clean_set_auto_interval_time(uart_port_t uart_num, uint16_t interval_time)
{
    if(chemins_uart_write_data(uart_num ,DEVICE_ADDRESS_CLEAN200 ,0x2020, interval_time))
    {
        ESP_LOGI(TAG_CLEAN200, "set [%d] auto interval time successful", interval_time);
        return eCHEMINS_OK;
    }
    else{
        ESP_LOGE(TAG_CLEAN200, "set auto interval time failure");
        return eCHEMINS_FAIL;
    }
}
CHEMINS_STATE clean_set_auto_revolution(uart_port_t uart_num, uint8_t number_of_turns)
{
    if(chemins_uart_write_data(uart_num, DEVICE_ADDRESS_CLEAN200, 0x1301, number_of_turns))
    {
        ESP_LOGI(TAG_CLEAN200, "set [%d] auto revolution successful", number_of_turns);
        return eCHEMINS_OK;
    }
    else
    {
        ESP_LOGE(TAG_CLEAN200, "set auto revolution failure");
        return eCHEMINS_FAIL;
    }
}

CHEMINS_STATE clean_set_device_address(uart_port_t uart_num, uint16_t device_address)
{
    if(chemins_uart_write_data(uart_num, DEVICE_ADDRESS_CLEAN200, 0x2002, device_address))
    {
        ESP_LOGI(TAG_CLEAN200, "set [%d] device address successful", device_address);
        return eCHEMINS_OK;
    }
    else
    {
        ESP_LOGE(TAG_CLEAN200, "set device address failure");
        return eCHEMINS_FAIL;
    }
}

uint8_t clean_get_device_address(uart_port_t uart_num)
{
    if(chemins_uart_read_data(uart_num, DEVICE_ADDRESS_CLEAN200, 0x2002, 0x0001))
    {
        ESP_LOGI(TAG_CLEAN200, "get[%d] device address successful", chemins_value.Clean200.DeviceAddress);
        return chemins_value.Clean200.DeviceAddress;
    }
    else
    {
        ESP_LOGE(TAG_CLEAN200, "get device address failure");
        return 0; // Return Modbus error code for failure
    }
}

uint16_t clean_get_auto_interval_time(uart_port_t uart_num)
{
    if(chemins_uart_read_data(uart_num, DEVICE_ADDRESS_CLEAN200, 0x1300, 0x0000))
    {
        ESP_LOGI(TAG_CLEAN200, "get[%d] auto interval time successful", chemins_value.Clean200.IntervalTime);
        return chemins_value.Clean200.IntervalTime;
    }
    else
    {
        ESP_LOGE(TAG_CLEAN200, "get auto interval time failure");
        return 0;
    }
}

uint8_t clean_get_auto_revolution(uart_port_t uart_num)
{
    if(chemins_uart_read_data(uart_num, DEVICE_ADDRESS_CLEAN200, 0x1301, 0x0000))
    {
        ESP_LOGI(TAG_CLEAN200, "get [%d]auto revolution successful", chemins_value.Clean200.Revolution);
        return chemins_value.Clean200.Revolution;
    }
    else
    {
        ESP_LOGE(TAG_CLEAN200, "get auto revolution failure");
        return 0;
    }
}