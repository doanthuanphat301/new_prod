#ifndef SERVO_CLEAN200_H
#define SERVO_CLEAN200_H

#ifdef __cplusplus
extern "C" {
#endif

#pragma once

#include "global.h"
#include "crc16.h"
#include "common_chemins.h"
#include "register_common_chemins.h"

struct clean_200_value {
  uint8_t device_address;   /**< Device address */
  uint8_t revolution;       /**< Revolution */
  uint16_t interval_time;   /**< interval Time */
};

CHEMINS_STATE clean_reset_factory(uart_port_t uart_num);
CHEMINS_STATE clean_set_auto_interval_time(uart_port_t uart_num, uint16_t interval_time);
CHEMINS_STATE clean_set_auto_revolution(uart_port_t uart_num, uint8_t number_of_turns);
CHEMINS_STATE clean_set_device_address(uart_port_t uart_num, uint16_t device_address);
uint8_t clean_get_device_address(uart_port_t uart_num);
uint16_t clean_get_auto_interval_time(uart_port_t uart_num);
uint8_t clean_get_auto_revolution(uart_port_t uart_num);

#ifdef __cplusplus
}
#endif

#endif // CLEAN200_H