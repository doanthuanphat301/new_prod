#ifndef CRC16_H
#define CRC16_H

#include <stdint.h>
#include <stddef.h>

/**
 * @brief Calculate the CRC16 checksum for the given data.
 * 
 * This function calculates the CRC16 checksum using the polynomial 0xA001.
 * 
 * @param data Pointer to the data buffer.
 * @param length Length of the data buffer.
 * @return The calculated CRC16 checksum.
 */
uint16_t calculate_crc(uint8_t *data, size_t length);

#endif // CRC16_H