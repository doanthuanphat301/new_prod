#ifndef PUB_SUBCLIENT_H
#define PUB_SUBCLIENT_H

#include "global.h"
#include "common_chemins.h"
#include "nvs_store.h"
#include "wifi.h"
#include "esp_private/esp_clk.h"
typedef struct {
    EventGroupHandle_t EventManager;
    QueueHandle_t      PublishQueue;
} PublishDef;

typedef struct
{
    char Cmd[200];
    QueueHandle_t QueueTaskHandle;
}TaskHandler;

typedef struct
{
    EventGroupHandle_t EventManager;
    TaskHandler        *List;
    uint8_t            NumberOfLists;
}subscribehDef;

typedef enum
{
    eOK = 0,
    eFAIL=1
}MQTT_STATE;

void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data);
void parse_and_add_to_json(const char *input_string, cJSON *json_obj);
MQTT_STATE data_to_mqtt(char *data, char *topic,int delay_time_ms,int qos);
void mqtt_init(char *broker_uri, char *username, char *client_id,bool storage_use);
void subscribe_to_topic(char *topic,int qos);
void get_data_subscribe_topic( uint16_t *data);
void mqtt_subscriber(esp_mqtt_event_handle_t event);
void publish_task(void *arg);
void get_data_subscribe_task(void *arg);
void add_subscribe_task(const char *command_name,QueueHandle_t recv_queue);
void subscribe_task(void *arg);
MQTT_STATE data_sensor_to_mqtt(char *data, char *topic,int delay_time_ms,int qos,DataStore store);
MQTT_STATE publish_action(char *data, char *topic,int qos,DataStore pub_store);
void mqtt_disconnect(void);
void mqtt_reconnect(void);
void mqtt_set_id(char *broker_url,char *id);
void mqtt_storage_connect_information(esp_mqtt_client_config_t *config);
void publish_frequent_data(void *arg);
#endif // PUB_SUBCLIENT
