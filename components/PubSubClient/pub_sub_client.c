#include "pub_sub_client.h"

const char *MQTT_TAG = "MQTT";
esp_mqtt_client_handle_t g_mqtt_client;  // MQTT client handle
char g_buffer_sub[200];
static EventGroupHandle_t g_mqtt_event_group;
const int g_constant_ConnectBit = BIT0;
const int g_constant_PublishedBit = BIT1;
double g_salinity_value=0;
double g_last_salinity_value=0;
bool device_state_update = false;
TaskHandle_t mqtt_handler;
static TaskHandler g_subscribe_task_list[100];
static uint8_t g_number_of_subscribers_task=0;
device_parameter_t device_parameter;
int g_message_id_of_sensordata =0;
SemaphoreHandle_t g_publish_semophore;
char g_broker[50];
char g_id[50];
/**
 * @brief Handles MQTT events.
 *
 * This function is the event handler for MQTT-related events. It processes different
 * event types such as connection, disconnection, subscription, unsubscription, published data,
 * and error events. Depending on the event type, appropriate actions are taken (e.g., logging,
 * setting event group bits, parsing received data).
 *
 * @param[in] handler_args Unused (can be NULL).
 * @param[in] base The event base (unused in this context).
 * @param[in] event_id The MQTT event ID.
 * @param[in] event_data The event data containing relevant information.
 *
 * @note Make sure the MQTT client is properly initialized and started before using this handler.
 */
void ack_sending(char* topic)
{
    char requestId[50];
    sscanf(topic, "v1/devices/me/rpc/request/%s", requestId);
    ESP_LOGI(MQTT_TAG, "Extracted requestId: %s", requestId);

    // Tạo topic để gửi ACK
    char responseTopic[100];
    snprintf(responseTopic, sizeof(responseTopic), "v1/devices/me/rpc/response/%s", requestId);

    // Dữ liệu ACK để gửi lên ThingsBoard
    const char *ack_data = "{\"status\":\"1\"}";

    // Gửi ACK
    esp_mqtt_client_publish(g_mqtt_client, responseTopic, ack_data, 0, 1, 0);
    ESP_LOGI(MQTT_TAG, "Sent ACK to topic: %s", responseTopic);
}
void mqtt_event_handler(void *handler_args, esp_event_base_t base, int32_t event_id, void *event_data)
{
    esp_mqtt_event_handle_t event = event_data;
    switch ((esp_mqtt_event_id_t)event->event_id)
    {
        case MQTT_EVENT_CONNECTED:
            ESP_LOGI(MQTT_TAG, "MQTT_EVENT_CONNECTED");
            xEventGroupSetBits(g_mqtt_event_group,g_constant_ConnectBit);
            subscribe_to_topic("v1/devices/me/rpc/request/+",2);
            device_state_update = true;
            break;
        case MQTT_EVENT_DISCONNECTED:
            xEventGroupClearBits(g_mqtt_event_group,g_constant_ConnectBit);
            if(checkwifi_state() == eWIFI_CONNECTED)
            {
                ESP_LOGI(MQTT_TAG, "MQTT_EVENT_DISCONNECTED");
                // Print the disconnection reason
                const char* disconnect_reason;
                switch (event->error_handle->error_type)
                {
                    case MQTT_ERROR_TYPE_NONE:
                        disconnect_reason = "No error";
                        break;
                    case MQTT_ERROR_TYPE_TCP_TRANSPORT:
                        disconnect_reason = "TCP Transport layer error";
                        esp_err_t err = event->error_handle->esp_tls_last_esp_err;
                        if (err != 0)
                        {
                            ESP_LOGE(MQTT_TAG, "Last esp error code: 0x%x, %s", err, esp_err_to_name(err));
                        }
                        ESP_LOGE(MQTT_TAG,"Error tls: %d\n", event->error_handle->esp_tls_stack_err);
                        ESP_LOGE(MQTT_TAG,"Error sock: %d\n", event->error_handle->esp_transport_sock_errno);
                        break;
                    case MQTT_ERROR_TYPE_CONNECTION_REFUSED:
                        disconnect_reason = "MQTT Connection refused error";
                        switch (event->error_handle->connect_return_code)
                        {
                            case MQTT_CONNECTION_ACCEPTED:
                                disconnect_reason = "Connection accepted";
                                break;
                            case MQTT_CONNECTION_REFUSE_PROTOCOL:
                                disconnect_reason = "Wrong protocol version";
                                break;
                            case MQTT_CONNECTION_REFUSE_ID_REJECTED:
                                disconnect_reason = "Client ID rejected";
                                break;
                            case MQTT_CONNECTION_REFUSE_SERVER_UNAVAILABLE:
                                disconnect_reason = "Server unavailable";
                                break;
                            case MQTT_CONNECTION_REFUSE_BAD_USERNAME:
                                disconnect_reason = "Bad username or password";
                                break;
                            case MQTT_CONNECTION_REFUSE_NOT_AUTHORIZED:
                                disconnect_reason = "Client not authorized";
                                break;
                            default:
                                disconnect_reason = "Unknown reason";
                                break;
                        }
                        break;
                    default:
                        disconnect_reason = "Unknown error type";
                        break;
                }
                ESP_LOGI(MQTT_TAG, "Disconnect reason: %s", disconnect_reason);
            }
            break;
        case MQTT_EVENT_SUBSCRIBED:
            ESP_LOGI(MQTT_TAG, "MQTT_EVENT_SUBSCRIBED");
            break;
        case MQTT_EVENT_UNSUBSCRIBED:
            ESP_LOGI(MQTT_TAG, "MQTT_EVENT_UNSUBSCRIBED");
            break;
        case MQTT_EVENT_PUBLISHED:
            if(g_message_id_of_sensordata>0 && event->msg_id ==g_message_id_of_sensordata)
            {
                xEventGroupSetBits(g_mqtt_event_group,g_constant_PublishedBit);
                g_message_id_of_sensordata=0;
                ESP_LOGW(MQTT_TAG,"Publish successfully key:%"PRIu16"\n",nvs_get_tail()-1);
            }
            else
            {
                ESP_LOGI(MQTT_TAG, "MQTT_EVENT_PUBLISHED");
            }
            break;
        case MQTT_EVENT_DATA:
            ESP_LOGI(MQTT_TAG, "MQTT_RECEIVED DATA");
            printf("DATA=%.*s\r\n", event->data_len, event->data);
            ack_sending(event->topic);
            mqtt_subscriber(event);
            break;
        case MQTT_EVENT_ERROR:
            if(checkwifi_state() == eWIFI_CONNECTED)
            {
                ESP_LOGE(MQTT_TAG, "MQTT_EVENT_ERROR");
                esp_mqtt_connect_return_code_t error_code = event->error_handle->connect_return_code;
                switch(error_code)
                {
                    case MQTT_CONNECTION_ACCEPTED:
                        ESP_LOGE(MQTT_TAG, "Connection accepted");
                        break;
                    case MQTT_CONNECTION_REFUSE_PROTOCOL:
                        ESP_LOGE(MQTT_TAG, "Connection refused, unacceptable protocol version");
                        break;
                    case MQTT_CONNECTION_REFUSE_SERVER_UNAVAILABLE:
                        ESP_LOGE(MQTT_TAG, "Connection refused, server unavailable");
                        break;
                    case MQTT_CONNECTION_REFUSE_BAD_USERNAME:
                        ESP_LOGE(MQTT_TAG, "Connection refused, bad username or password");
                        break;
                    case MQTT_CONNECTION_REFUSE_NOT_AUTHORIZED:
                        ESP_LOGE(MQTT_TAG, "Connection refused, not authorized");
                        break;
                    default:
                        ESP_LOGE(MQTT_TAG, "Connection refused, unknown reason");
                        break;
                }
                esp_mqtt_error_type_t error_type = event->error_handle->error_type;
                switch(error_type)
                {
                    case MQTT_ERROR_TYPE_NONE:
                        ESP_LOGE(MQTT_TAG, "No error");
                        break;
                    case MQTT_ERROR_TYPE_CONNECTION_REFUSED:
                        ESP_LOGE(MQTT_TAG, "Connection refused");
                        break;
                    case MQTT_ERROR_TYPE_TCP_TRANSPORT:
                        ESP_LOGE(MQTT_TAG, "TCP transport error");
                        break;
                    case MQTT_ERROR_TYPE_SUBSCRIBE_FAILED:
                        ESP_LOGE(MQTT_TAG, "Subscribe failed");
                        break;
                    default:
                        ESP_LOGE(MQTT_TAG, "Unknown error type");
                        break;
                }
                esp_err_t err = event->error_handle->esp_tls_last_esp_err;
                if (err != 0)
                {
                    ESP_LOGE(MQTT_TAG, "Last esp error code: 0x%x, %s", err, esp_err_to_name(err));
                }
                ESP_LOGE(MQTT_TAG,"Error tls: %d\n", event->error_handle->esp_tls_stack_err);
                ESP_LOGE(MQTT_TAG,"Error sock: %d\n", event->error_handle->esp_transport_sock_errno);
            }
            break;
        default:
            ESP_LOGI(MQTT_TAG, "Other event id:%d", event->event_id);
            break;
    }
}

/**
 * @brief Initializes and starts the MQTT client.
 *
 * This function sets up the MQTT client with the provided broker URI and username.
 * It creates an event group, initializes the client configuration, registers an event handler,
 * and starts the MQTT client.
 *
 * @param[in] broker_uri The MQTT broker URI.
 * @param[in] username The username for authentication (if required).
 *
 * @note Make sure to call this function after configuring the MQTT parameters.
 */
void mqtt_init(char *broker_uri, char *username, char *client_id,bool storage_use)
{
    uint8_t storage_exists = 0;
    g_mqtt_event_group = xEventGroupCreate();
    if(g_mqtt_event_group!=NULL)
    {
        ESP_LOGI(MQTT_TAG, "Event group for MQTT created successfully");
    }
    else
    {
        ESP_LOGE(MQTT_TAG, "Failed to create event group");
    }

    if(storage_use ==true)
    {
        nvs_handle_t my_handle;
        esp_err_t err =nvs_open_from_partition("nvs_data","mqtt", NVS_READWRITE, &my_handle);
        if (err != ESP_OK)
        {
            printf("Error opening NVS handle!\n");
        }
        else
        {
            size_t required_size;
            nvs_get_str(my_handle, "mqtt_broker", NULL, &required_size);
            err = nvs_get_str(my_handle, "mqtt_broker", g_broker,&required_size);
            switch (err) {
                case ESP_OK:
                    printf("The mqtt information is initialized!\n");
                    storage_exists++;
                    break;
                case ESP_ERR_NVS_NOT_FOUND:
                    printf("The mqtt information is not initialized yet!\n");
                    break;
                default :
                    printf("Error reading!\n");
            }

            nvs_get_str(my_handle, "mqtt_id", NULL, &required_size);
            err = nvs_get_str(my_handle, "mqtt_id", g_id,&required_size);
            switch (err) {
                case ESP_OK:
                    printf("The mqtt information is initialized!\n");
                    storage_exists++;
                    break;
                case ESP_ERR_NVS_NOT_FOUND:
                    printf("The mqtt information is not initialized yet!\n");
                    break;
                default :
                    printf("Error reading!\n");
            }
        }
        nvs_close(my_handle);
    }
    if(storage_exists == 2)
    {
        esp_mqtt_client_config_t mqtt_cfg =
        {
            .broker.address.uri = g_broker,  // MQTT broker URI from configuration
            .credentials.username = g_id,
            //.credentials.set_null_client_id = true,
            .session.keepalive = 30
            //.network.reconnect_timeout_ms = 1,
            //.network.refresh_connection_after_ms = 60000
        };
        ESP_LOGI(MQTT_TAG,"broker_uri=%s", g_broker);
        ESP_LOGI(MQTT_TAG,"id=%s", g_id);
        g_mqtt_client = esp_mqtt_client_init(&mqtt_cfg);  // Initialize the MQTT client
        esp_mqtt_client_register_event(g_mqtt_client, ESP_EVENT_ANY_ID, mqtt_event_handler,NULL);  // Register the event handler
        esp_mqtt_client_start(g_mqtt_client);
    }
    else
    {
        esp_mqtt_client_config_t mqtt_cfg =
        {
            .broker.address.uri = broker_uri,  // MQTT broker URI from configuration
            .credentials.username = username,
            //.credentials.set_null_client_id = true,
            .session.keepalive = 30
            //.network.reconnect_timeout_ms = 1,
            //.network.refresh_connection_after_ms = 60000
        };
        strcpy(g_broker,broker_uri);
        ESP_LOGI(MQTT_TAG,"broker_uri=%s", g_broker);
        strcpy(g_id,username);
        ESP_LOGI(MQTT_TAG,"id=%s", g_id);
        g_mqtt_client = esp_mqtt_client_init(&mqtt_cfg);  // Initialize the MQTT client
        esp_mqtt_client_register_event(g_mqtt_client, ESP_EVENT_ANY_ID, mqtt_event_handler,NULL);  // Register the event handler
        esp_mqtt_client_start(g_mqtt_client);
        mqtt_storage_connect_information(&mqtt_cfg);
    }
    vTaskDelay(1000/portTICK_PERIOD_MS);
}
int checkString( const char s[] )
{
    int dot_count = 0;
    for(int i = 0; i<strlen(s); ++i)
    {
        if( (s[i] >= 'A' && s[i] <= 'Z') || (s[i] >= 'a' && s[i] <= 'z') || (s[i] == '.' && dot_count > 0) )
        {
            return 0; //is string
        }
        else if(s[i] == '.')
        {
            dot_count++;
        }
    }
    return 1; //is number
}
void parse_and_add_to_json(const char *input_string, cJSON *json_obj)
{
    char *input_copy = strdup(input_string);
    if (input_copy == NULL)
    {
        printf("Failed to allocate memory.\n");
        return;
    }
    char *obj;
    char *val;
    obj = strtok(input_copy, ": ");
    val = strtok(NULL, ",");
    if(*val == ' ')
    {
        val++;
    }
    if(checkString(val) == 1)
    {
        cJSON_AddNumberToObject(json_obj, obj, strtod(val,NULL));
    }
    else if(checkString(val) == 0)
    {
        cJSON_AddStringToObject(json_obj, obj, val);
    }
    while(obj!=NULL && val!=NULL)
    {
        obj = strtok(NULL, ": ");
        val = strtok(NULL, ",");
        if(obj!=NULL && val!=NULL)
        {
            if(*val == ' ')
            {
                val++;
            }
            if(checkString(val) == 1)
            {
                cJSON_AddNumberToObject(json_obj, obj, strtod(val,NULL));
            }
            else if(checkString(val) == 0)
            {
                cJSON_AddStringToObject(json_obj, obj, val);
            }
        }
    }    // Free the copy of input string
    free(input_copy);
}

void mqtt_subscriber(esp_mqtt_event_handle_t event)
{
    strncpy(g_buffer_sub,event->data,event->data_len);
    xTaskNotifyGive(mqtt_handler);
}
MQTT_STATE data_to_mqtt(char *data, char *topic, int delay_time_ms, int qos)
{
    if(xEventGroupWaitBits(g_mqtt_event_group,g_constant_ConnectBit,pdFALSE,pdTRUE,(TickType_t )50)& g_constant_ConnectBit )
    {
        ESP_LOGW(MQTT_TAG,"Broker connected , ready publish............");
    }
    else
    {
        ESP_LOGE(MQTT_TAG,"Broker not connected...........................");
        return eFAIL;
    }
    int len = strlen(data);
    if (len > 0)
    {
        //data[len] = '\0';  // Null-terminate the received data
        // Publish the data to the MQTT broker
        cJSON *json_obj = cJSON_CreateObject();
        if (json_obj == NULL)
        {
            printf("Failed to create JSON object.\n");
            return eFAIL;
        }
        parse_and_add_to_json(data,json_obj);
        char *json_data = cJSON_Print(json_obj);
        int ret=esp_mqtt_client_publish(g_mqtt_client, topic, json_data, strlen(json_data), qos, 0);
        if (ret == -1)
        {
            ESP_LOGE(MQTT_TAG, "Failed to publish data!");
            cJSON_Delete(json_obj);
            free(json_data);
            return eFAIL;
        }
        else if (ret == -2)
        {
            ESP_LOGW(MQTT_TAG, "Data buffer full!");
            cJSON_Delete(json_obj);
            free(json_data);
            return eFAIL;
        }
        else
        {
            //printf("Message sent: %s \n", json_data);
            ESP_LOGI(MQTT_TAG,"Message sent: %s", json_data);
            cJSON_Delete(json_obj);
            free(json_data);
        }
    }
    vTaskDelay(delay_time_ms/portTICK_PERIOD_MS);
    return eOK;
}
/**
 * @brief Subscribes to an MQTT topic with the specified quality of service (QoS).
 *
 * This function subscribes to an MQTT topic using the provided client and topic name.
 * If the subscription fails, an error message is logged.
 *
 * @param[in] topic The topic to subscribe to.
 * @param[in] qos The desired quality of service (0, 1, or 2).
 *
 * @note Make sure the MQTT client is connected before calling this function.
 */
void subscribe_to_topic(char *topic,int qos)
{
    xEventGroupWaitBits(g_mqtt_event_group,g_constant_ConnectBit,false,true,portMAX_DELAY);
    if(esp_mqtt_client_subscribe(g_mqtt_client,topic,qos) == -1)
    {
        ESP_LOGE(MQTT_TAG,"Failed to subscribe to topic");
    }
}
void get_data_subscribe_topic( uint16_t *data)
{
    *data=(uint16_t)(g_salinity_value*10);
}
bool data_is_valid(float data)
{
    if(data != DATA_ERROR)
    {
        return true;
    }
    return false;
}
void create_sensor_string(SensorsData data, char *output, size_t size) {
    char buffer[100] = {0};
    int len = 0;

    if (data_is_valid(data.Rdo206AValue.DO)) {
        len += snprintf(buffer + len, sizeof(buffer) - len, "do: %.2f, ", data.Rdo206AValue.DO);
    }
    if (data_is_valid(data.Phg206AValue.PH)) {
        len += snprintf(buffer + len, sizeof(buffer) - len, "ph: %.2f, ", data.Phg206AValue.PH);
    }
    if (data_is_valid(data.Rdo206AValue.Temp)) {
        len += snprintf(buffer + len, sizeof(buffer) - len, "rtd: %.1f, ", data.Rdo206AValue.Temp);
    }
    if (data_is_valid(data.Phg206AValue.Temp)) {
        len += snprintf(buffer + len, sizeof(buffer) - len, "rtd_box: %.1f, ", data.Phg206AValue.Temp);
    }

    // // Xóa dấu phẩy và khoảng trắng cuối cùng nếu có
    // if (len > 2 && buffer[len - 2] == ',') {
    //     buffer[len - 2] = '\0';
    // }

    snprintf(buffer+len,sizeof(buffer)-len,"timestamp: %" PRIu32"", data.TimeStamp);

    // Sao chép chuỗi kết quả vào output
    snprintf(output, size, "%s", buffer);
}
void publish_task(void *arg)
{
    mqtt_init("mqtt://35.240.204.122:1883", g_envisor_id, NULL,true);
    PublishDef *def = (PublishDef *)arg;
    SensorsData sensor;
    BaseType_t ret;
    char data[100];
    DataStore store_data;
    while(1)
    {
        if(device_state_update == true)
        {
            get_device_init_parameter(&device_parameter);
            char device_init_data[200];
            snprintf(device_init_data, sizeof(device_init_data), "internet: %s, mac: %s, ip_address: %s, bootnum: %d, firmware_version: %s", device_parameter.internet, device_parameter.mac, device_parameter.ip_address, device_parameter.bootnum, device_parameter.firmware_version);
            data_to_mqtt(device_init_data, "v1/devices/me/attributes",0, 2);
            device_state_update = false;
        }
        ret=xQueueReceive(def->PublishQueue,&sensor,(TickType_t)portMAX_DELAY);
        xEventGroupClearBits(def->EventManager, FINISH_PUBLISH_EVENT);
        if(ret==pdTRUE)
        {
            ESP_LOGI(MQTT_TAG,"Receive data from queue successfully");
#ifdef  NOT_STORE_DATA_TO_NVS
            sprintf(data, "rtd: %.1f, do: %.2f, rtd_box: %.1f, ph: %.2f", sensor.Rdo206AValue.Temp, sensor.Rdo206AValue.DO, sensor.Phg206AValue.Temp, sensor.Phg206AValue.PH);
            data_to_mqtt(data, "v1/devices/me/telemetry",500, 1);
#else
            // sprintf(data, "temperature_rdo: %.1f, do: %.2f, temperature_phg: %.1f, ph: %.2f, timestamp: %" PRIu32"", sensor.Rdo206AValue.Temp, sensor.Rdo206AValue.DO, sensor.Phg206AValue.Temp, sensor.Phg206AValue.PH,sensor.TimeStamp);
            // sprintf(data, "rtd: %.1f, do: %.2f, rtx_box: %.1f, ph: %.2f, timestamp: %" PRIu32"", sensor.Rdo206AValue.Temp, sensor.Rdo206AValue.DO, sensor.Phg206AValue.Temp, sensor.Phg206AValue.PH, sensor.TimeStamp);
            create_sensor_string(sensor,data,(size_t)100);
            store_data = nvs_transfer_data_to_storage(sensor);
            data_sensor_to_mqtt(data, "v1/devices/me/telemetry",0, 2, store_data);
#endif
            // get_device_frequent_parameter(&device_parameter);
            // snprintf(device_frequent_data, sizeof(device_frequent_data), "uptime: %.2f, rssi: %d", device_parameter.uptime, device_parameter.rssi);
            // data_to_mqtt(device_frequent_data, "v1/devices/me/attributes",0, 2);
            xEventGroupSetBits(def->EventManager, FINISH_PUBLISH_EVENT);
            uint32_t cpu_freq_hz = esp_clk_cpu_freq();

            // In tần số CPU ra console (đơn vị là MHz)
            ESP_LOGI(MQTT_TAG, "CPU frequency: %" PRIu32"", cpu_freq_hz / 1000000);
        }
    }
}
void publish_frequent_data(void *arg)
{
    char device_frequent_data[100];
    float cpu_temperature;
    while(1)
    {
        xEventGroupWaitBits(g_mqtt_event_group,g_constant_ConnectBit,false,true,portMAX_DELAY);
        get_device_frequent_parameter(&device_parameter);
        get_cpu_temp(&cpu_temperature);
        snprintf(device_frequent_data, sizeof(device_frequent_data), "uptime: %.2f, rssi: %d, cpu_temp: %.2f", device_parameter.uptime, device_parameter.rssi,cpu_temperature);
        data_to_mqtt(device_frequent_data, "v1/devices/me/attributes", 10000, 1);
        uint32_t cpu_freq_hz = esp_clk_cpu_freq();

        // In tần số CPU ra console (đơn vị là MHz)
        ESP_LOGI(MQTT_TAG, "CPU frequency: %" PRIu32 "", cpu_freq_hz / 1000000);
    }
}

void add_subscribe_task(const char *command_name,QueueHandle_t recv_queue)
{
    g_subscribe_task_list[g_number_of_subscribers_task].QueueTaskHandle = recv_queue;
    strcpy(g_subscribe_task_list[g_number_of_subscribers_task].Cmd,command_name);
    g_number_of_subscribers_task++;
}

void get_data_subscribe_task(void *arg)
{
    // subscribehDef *def = (subscribehDef*)arg;
    cJSON *params;
    char *cmd;
    while(1)
    {
        ulTaskNotifyTake(pdTRUE,(TickType_t)portMAX_DELAY);
        cJSON* data_sub=cJSON_Parse(g_buffer_sub);
        params=cJSON_GetObjectItem(data_sub,"params");
        char *my_json_string = cJSON_Print(params);
        cJSON_free(my_json_string);
        char *params_string =cJSON_GetObjectItem(params,"cmd")->valuestring;
        cmd =strtok(params_string,"/");
        printf("command line: %s\n",cmd);
        for(int i=0;i<g_number_of_subscribers_task;i++)
        {
            if(strcmp(cmd,g_subscribe_task_list[i].Cmd)==0)
            {
                char *content_of_cmd = strtok(NULL,"");
                printf("cmd: %s\n",content_of_cmd);
                if(xQueueSend(g_subscribe_task_list[i].QueueTaskHandle,content_of_cmd,(TickType_t)100)==pdTRUE)
                {
                        ESP_LOGI(MQTT_TAG,"Send data to subscribe queue successfully");
                }
            }
        }
        cJSON_Delete(data_sub);
    }
}

void subscribe_task(void *arg)
{
    xTaskCreate(get_data_subscribe_task, "subscribe_task", 1024*6, arg,10, &mqtt_handler);
}

MQTT_STATE data_sensor_to_mqtt(char *data, char *topic,int delay_time_ms,int qos,DataStore store)
{

    if(xEventGroupWaitBits(g_mqtt_event_group,g_constant_ConnectBit,pdFALSE,pdTRUE,(TickType_t )50)& g_constant_ConnectBit )
    {
        ESP_LOGW(MQTT_TAG,"Broker connected , ready publish............");
        char nvs_data[100];
        DataStore nvs_data_store;;
        xEventGroupClearBits(g_mqtt_event_group,g_constant_PublishedBit);
        while(nvs_load(&nvs_data_store) ==eNVS_READY )
        {
            if((xEventGroupWaitBits(g_mqtt_event_group,g_constant_ConnectBit,pdFALSE,pdTRUE,(TickType_t )500)& g_constant_ConnectBit)&&(checkwifi_state()==eWIFI_CONNECTED) )
            {
                xEventGroupClearBits(g_mqtt_event_group,g_constant_PublishedBit);
                ESP_LOGW(MQTT_TAG,"Data exist in NVS, ready publish............");
                SensorsData nvs_sensor=nvs_transfer_storage_to_data(nvs_data_store);
                create_sensor_string(nvs_sensor,nvs_data,(size_t)100);
                publish_action(nvs_data,topic,qos,nvs_data_store);
                if(xEventGroupWaitBits(g_mqtt_event_group,g_constant_PublishedBit,pdTRUE,pdTRUE,(TickType_t )800/portTICK_PERIOD_MS) &g_constant_PublishedBit)
                {
                    continue;
                }
                else
                {
                    ESP_LOGE(MQTT_TAG,"Failed to publish data to MQTT broker.... Storing data");
                    nvs_store_again(nvs_data_store);
                    return eFAIL;
                }
            }
            else
            {
                ESP_LOGE(MQTT_TAG,"Broker not connected...........................");
                if( nvs_store_again(nvs_data_store)!=ESP_OK)
                {
                    ESP_LOGE(MQTT_TAG,"Failed to save data to NVS");
                    return eFAIL;
                }
                return eFAIL;
            }
        }
    }
    else
    {
        ESP_LOGE(MQTT_TAG,"Broker not connected...........................");
        if(nvs_store(store)!=ESP_OK)
        {
            ESP_LOGE(MQTT_TAG,"Failed to save data to NVS");
            return eFAIL;
        }
        return eFAIL;
    }
    if(publish_action(data,topic,qos,store) != eOK)
    {
        nvs_store_again(store);
        return eFAIL;
    }
    else
    {
        if(xEventGroupWaitBits(g_mqtt_event_group,g_constant_PublishedBit,pdTRUE,pdTRUE,(TickType_t )800/portTICK_PERIOD_MS) &g_constant_PublishedBit)
        {
            return eOK;
        }
        else
        {
            ESP_LOGE(MQTT_TAG,"Failed to publish data to MQTT broker.... Storing data");
            nvs_store_again(store);
            return eFAIL;
        }
    }
    vTaskDelay(delay_time_ms/portTICK_PERIOD_MS);
    return eOK;
}
MQTT_STATE publish_action(char *data, char *topic,int qos,DataStore pub_store)
{
    int len = strlen(data);
    if (len > 0)
    {
        data[len] = '\0';  // Null-terminate the received data
        // Publish the data to the MQTT broker
        cJSON *json_obj = cJSON_CreateObject();
        if (json_obj == NULL)
        {
            printf("Failed to create JSON object.\n");
            return eFAIL;
        }
        parse_and_add_to_json(data,json_obj);
        char *json_data = cJSON_Print(json_obj);
        g_message_id_of_sensordata=esp_mqtt_client_publish(g_mqtt_client, topic, json_data, strlen(json_data), qos, 0);
        if (g_message_id_of_sensordata == -1)
        {
            ESP_LOGE(MQTT_TAG, "Failed to publish data!");
            cJSON_Delete(json_obj);
            free(json_data);
            if(nvs_store_again(pub_store)!=ESP_OK)
            {
                ESP_LOGE(MQTT_TAG,"Failed to save data to NVS");
                return eFAIL;
            }
            return eFAIL;
        }
        else if (g_message_id_of_sensordata == -2)
        {
            ESP_LOGW(MQTT_TAG, "Data buffer full!");
            cJSON_Delete(json_obj);
            free(json_data);
            if(nvs_store_again(pub_store)!=ESP_OK)
            {
                ESP_LOGE(MQTT_TAG,"Failed to save data to NVS");
                return eFAIL;
            }
            return eFAIL;
        }
        else
        {
            //printf("Message sent: %s", json_data);
            /*test net-logging*/
            ESP_LOGI(MQTT_TAG,"Message sent: %s", json_data);
            /*test net-logging*/
            cJSON_Delete(json_obj);
            free(json_data);
            return eOK;
        }
        // if(xEventGroupWaitBits(g_mqtt_event_group,g_constant_PublishedBit,pdTRUE,pdTRUE,(TickType_t )500/portTICK_PERIOD_MS) &g_constant_PublishedBit)
        // {
        //     return eOK;
        // }
        // else
        // {
        //     ESP_LOGE(MQTT_TAG,"Failed to publish data to MQTT broker.... Storing data");
        //     nvs_store_again(pub_store);
        //     return eFAIL;
        // }
    }
    return eFAIL;
}

void mqtt_disconnect(void)
{
    esp_mqtt_client_disconnect(g_mqtt_client);
}

void mqtt_reconnect(void)
{
    esp_mqtt_client_reconnect(g_mqtt_client);
}

void mqtt_set_id(char *broker_url,char *id)
{
    mqtt_disconnect();
    xEventGroupClearBits(g_mqtt_event_group,g_constant_ConnectBit);
    esp_mqtt_client_config_t mqtt_cfg =
    {
        .broker.address.uri = broker_url,  // MQTT broker URI from configuration
        .credentials.username = id,
        //.credentials.set_null_client_id = true,
        .session.keepalive = 30
        //.network.reconnect_timeout_ms = 1,
        //.network.refresh_connection_after_ms = 60000
    };
    esp_mqtt_set_config(g_mqtt_client,&mqtt_cfg);
    esp_mqtt_client_reconnect(g_mqtt_client);
    if(xEventGroupWaitBits(g_mqtt_event_group,g_constant_ConnectBit,pdFALSE,pdTRUE,20000/portTICK_PERIOD_MS)& g_constant_ConnectBit )
    {
        ESP_LOGW(MQTT_TAG,"Connect to new mqtt server successfully");
        if(UDP_ENABLE)
        {
            printf("%d\n",id_to_port(id));
            udp_logging_init(CONFIG_LOG_UDP_SERVER_IP, id_to_port(id), udp_logging_vprintf);
        }
        mqtt_storage_connect_information(&mqtt_cfg);
    }
    else
    {
        ESP_LOGE(MQTT_TAG, "Failed to connect to new broker");
        ESP_LOGW(MQTT_TAG, "Prepare to connect to older broker");
        ESP_LOGI(MQTT_TAG,"Broker: %s ,id: %s",g_broker,g_id);
        mqtt_disconnect();
        esp_mqtt_client_config_t re_config =
        {
            .broker.address.uri = g_broker,  // MQTT broker URI from configuration
            .credentials.username = g_id,
            //.credentials.set_null_client_id = true,
            .session.keepalive = 30
            //.network.reconnect_timeout_ms = 1,
            //.network.refresh_connection_after_ms = 60000
        };
        esp_mqtt_set_config(g_mqtt_client,&re_config);
        esp_mqtt_client_reconnect(g_mqtt_client);
    }
}

void mqtt_storage_connect_information(esp_mqtt_client_config_t *config)
{
    nvs_handle_t nvs_handle;
    esp_err_t err;

    // Initialize NVS
    err =nvs_open_from_partition("nvs_data","mqtt", NVS_READWRITE, &nvs_handle);
    if (err != ESP_OK)
    {
        ESP_LOGE(MQTT_TAG, "Error opening NVS handle!");
        return;
    }
    // Save the updated configurations
    printf("broker:%s id:%s \n",config->broker.address.uri,config->credentials.username);
    err = nvs_set_str(nvs_handle, "mqtt_broker", config->broker.address.uri);
    if (err != ESP_OK) {
        ESP_LOGE(MQTT_TAG, "Error saving to NVS!");
        nvs_close(nvs_handle);
        return;
    }

    err = nvs_set_str(nvs_handle, "mqtt_id", config->credentials.username);
    if (err != ESP_OK) {
        ESP_LOGE(MQTT_TAG, "Error saving to NVS!");
        nvs_close(nvs_handle);
        return;
    }

    err = nvs_commit(nvs_handle);
    if (err != ESP_OK) {
        ESP_LOGE(MQTT_TAG, "Error committing to NVS!");
    }

    nvs_close(nvs_handle);
}